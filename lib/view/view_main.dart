import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:nearestdistrict_app/bloc/main_bloc.dart';
import 'package:nearestdistrict_app/bloc/states/main_state.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/models/user.dart';
import 'package:nearestdistrict_app/service/geo_location_service.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';
import 'package:nearestdistrict_app/utils/sp_keys.dart';
import 'package:nearestdistrict_app/view/templetes/view_header.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ViewMain extends StatelessWidget {
  ViewMain({Key? key}) : super(key: key);

  final TextStyle _textStyle = TextStyle(
    color: Colors.black,
    fontSize: 14,
  );
  final TextStyle _textStyleHeader = TextStyle(
    color: Colors.white,
    fontSize: 20,
  );

  late User _userInfo;
  late Position _position;

  void _getCurrentPosition() async {
    _position = await GeoLocationService.determinePosition();
    await inject<SharedPreferences>()
        .setDouble(SPKeys.userCurrentPositionLat, _position.latitude);
    await inject<SharedPreferences>()
        .setDouble(SPKeys.userCurrentPositionLon, _position.longitude);
  }

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> map =
        jsonDecode(inject<SharedPreferences>().getString(SPKeys.userInfo)!);
    _userInfo = User.fromJson(map);
    _getCurrentPosition();

    return Scaffold(
      appBar: AppBar(
        title: Text(AppStrings.titleViewMain),
      ),
      drawer: Drawer(
        child: _setContentDrawer(context),
      ),
      body: BlocProvider(
        create: (context) => MainBloc(),
        child: BlocBuilder<MainBloc, MainState>(
          builder: (context, state) {
            if (state is MainSetTotal) {
              return Padding(
                padding: EdgeInsets.all(16.0),
                child: Center(
                  child: Column(
                    children: [
                      ViewHeader(),
                      _setMainBtns(
                        label: AppStrings.btnMainNewCacheBack,
                        fun: () {
                          Navigator.of(context).pushNamed('new cashback');
                        },
                      ),
                      _setMainBtns(
                        label: AppStrings.btnMainMyChoice,
                        fun: state.totals.offers == 0
                            ? null
                            : () {
                                Navigator.of(context).pushNamed('my offers');
                              },
                        needSuf: true,
                        suffixValue: state.totals.offers,
                      ),
                      _setMainBtns(
                        label: AppStrings.btnMainMyScore,
                        fun: state.totals.wallet == null ||
                                state.totals.wallet == 0
                            ? null
                            : () {
                                Navigator.of(context).pushNamed('my score');
                              },
                        needSuf: true,
                        suffixValue: state.totals.wallet != null
                            ? state.totals.wallet! ~/ 100
                            : 0,
                      ),
                      _setMainBtns(
                        label: AppStrings.btnMainSearch,
                        fun: () {
                          Navigator.of(context).pushNamed('find');
                        },
                      ),
                      _setMainBtns(
                        label: AppStrings.btnMainMyRealWallet,
                        fun: () {
                          Navigator.of(context).pushNamed('my cash');
                        },
                        needSuf: true,
                        suffixValue: state.totals.realWallet ~/ 100,
                      ),
                      _setTextAnot(),
                    ],
                  ),
                ),
              );
            }
            return Padding(
              padding: EdgeInsets.all(16.0),
              child: Center(
                child: Column(
                  children: [
                    ViewHeader(),
                    _setMainBtns(
                      label: AppStrings.btnMainNewCacheBack,
                      fun: () {
                        Navigator.of(context).pushNamed('new cashback');
                      },
                    ),
                    _setMainBtns(
                      label: AppStrings.btnMainMyChoice,
                      fun: null,
                      needSuf: true,
                    ),
                    _setMainBtns(
                      label: AppStrings.btnMainMyScore,
                      fun: null,
                      needSuf: true,
                    ),
                    _setMainBtns(
                      label: AppStrings.btnMainSearch,
                      fun: () {
                        Navigator.of(context).pushNamed('find');
                      },
                    ),
                    _setMainBtns(
                      label: AppStrings.btnMainMyRealWallet,
                      fun: () {
                        Navigator.of(context).pushNamed('my cash');
                      },
                      needSuf: true,
                    ),
                    _setTextAnot(),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _setMainBtns({
    String label = "",
    void Function()? fun,
    bool? needSuf = false,
    int? suffixValue,
  }) {
    return GestureDetector(
      onTap: fun != null ? fun : null,
      child: Padding(
        padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
        child: Container(
          height: 40,
          decoration: BoxDecoration(
            color: fun != null ? Colors.green : Colors.grey,
            borderRadius: BorderRadius.all(Radius.circular(5)),
          ),
          child: Center(
            child: needSuf == false
                ? Text(
                    label,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        suffixValue != null ? label + "($suffixValue)" : label,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      suffixValue == null
                          ? Container(
                              margin: EdgeInsets.only(left: 8.0),
                              width: 20,
                              height: 20,
                              child: CircularProgressIndicator(
                                color: Colors.white,
                              ),
                            )
                          : Container(),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  Widget _setContentDrawer(BuildContext context) {
    return ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          decoration: BoxDecoration(
            color: Colors.green[800],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                _userInfo.firstName,
                style: _textStyleHeader,
              ),
              Text(
                _userInfo.phone,
                style: _textStyleHeader,
              ),
            ],
          ),
        ),
        ListTile(
          title: Text(AppStrings.btnMainExit),
          onTap: () {
            inject<SharedPreferences>().setBool(SPKeys.userLogged, false);
            Navigator.of(context).pushReplacementNamed('login');
          },
        ),
      ],
    );
  }

  Widget _setTextAnot() {
    return Flexible(
      child: RichText(
        text: TextSpan(
          style: _textStyle,
          children: [
            TextSpan(
              text: AppStrings.mainActivityAdditionalLabelBig1,
            ),
            TextSpan(
              text: AppStrings.mainActivityAdditionalLabelBig2,
            ),
            TextSpan(
              text: AppStrings.mainActivityAdditionalLabelBig3,
            ),
            TextSpan(
              text: AppStrings.mainActivityAdditionalLabelBig4,
            ),
          ],
        ),
      ),
    );
  }
}
