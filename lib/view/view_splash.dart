import 'package:flutter/material.dart';
import 'package:flutter_svg/avd.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/utils/sp_keys.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ViewSplash extends StatefulWidget {
  const ViewSplash({Key? key}) : super(key: key);

  @override
  _ViewSplashState createState() => _ViewSplashState();
}

class _ViewSplashState extends State<ViewSplash> {

  @override
  void initState() {
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _chooseView(),
      builder: (context, snapshot) {
        return Container(
          color: Colors.green[800],
          child: Padding(
            padding: EdgeInsets.all(48),
              child: AvdPicture.asset('assets/drawable/app_logo_new.xml'),
            ),
        );
      },
    );
  }

  Future<void> _chooseView() async {
    await Future.delayed(Duration(milliseconds: 5000));
    if (inject<SharedPreferences>().getBool(SPKeys.userLogged) == true) {
      Navigator.of(context).pushNamed('main');
    } else if (inject<SharedPreferences>().getBool(SPKeys.needIntro) == false) {
      Navigator.of(context).pushNamed('login');
    } else {
      Navigator.of(context).pushNamed('slider');
    }
  }
}
