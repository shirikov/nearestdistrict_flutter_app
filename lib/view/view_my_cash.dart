import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:nearestdistrict_app/bloc/events/my_cash_event.dart';
import 'package:nearestdistrict_app/bloc/my_cash_bloc.dart';
import 'package:nearestdistrict_app/bloc/states/my_cash_state.dart';
import 'package:nearestdistrict_app/models/cash_out_history.dart';
import 'package:nearestdistrict_app/models/yandex_money_out.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';
import 'package:nearestdistrict_app/view/templetes/view_header.dart';
import 'package:url_launcher/url_launcher.dart';

class ViewMyCash extends StatefulWidget {
  const ViewMyCash({Key? key}) : super(key: key);

  @override
  _ViewMyCashState createState() => _ViewMyCashState();
}

class _ViewMyCashState extends State<ViewMyCash> {
  bool isChecked = false;
  bool _checkPhoneOrCash = false;
  bool _checkSum = false;
  late TextEditingController _controllerPhone;
  late TextEditingController _controllerSum;
  Widget? _saveState;
  int _saveChoose = 0;

  var _maskPhoneFormatter = MaskTextInputFormatter(
    mask: '+7 (###) ### ## ##',
    filter: {'#': RegExp(r'[0-9]')},
  );

  @override
  void initState() {
    super.initState();
    _controllerPhone = TextEditingController();
    _controllerSum = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _getTitleView(),
      ),
      body: BlocProvider(
        create: (context) => MyCashBloc(),
        child: BlocConsumer<MyCashBloc, MyCashState>(
          listener: (context, state) {
            if (state is MyCashFailure) {
              BlocProvider.of<MyCashBloc>(context)
                  .add(MyCashGetTransfer(_saveChoose));
              showDialog(
                context: context,
                builder: (context) {
                  print(state.errorMessage);
                  return AlertDialog(
                    title: Text(AppStrings.error),
                    content: Text(state.errorMessage),
                  );
                },
              );
            }
          },
          builder: (context, state) {
            if (state is MyCashInitState) {
              return _setStateInit(state, context);
            }
            if (state is MyCashTransfer) {
              return _setStateTransferMoney(context);
            }
            if (state is MyCashTransferChoose) {
              if (state.selectionMethod == 0) {
                return _saveState = _setStateTransferByYandexMoney(context);
              } else {
                return _saveState = _setStateTransferByPhone(context);
              }
            }
            if (state is MyCashHistoryTransferReceived) {
              return _setStateHistoryTransfer(state.list);
            }
            return _saveState != null ? _saveState! : Container();
          },
        ),
      ),
    );
  }

  Widget _getTitleView() => Text(AppStrings.titleViewMyCash);

  Widget _setMainBtns({
    String label = "",
    void Function()? fun,
  }) {
    return GestureDetector(
      onTap: fun != null ? fun : null,
      child: Padding(
        padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
        child: Container(
          height: 40,
          decoration: BoxDecoration(
            color: fun != null ? Colors.green : Colors.grey,
            borderRadius: BorderRadius.all(Radius.circular(5)),
          ),
          child: Center(
            child: Text(
              label,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _setStateInit(MyCashInitState state, BuildContext context) {
    var textStyle = TextStyle(
      fontSize: 12,
      color: Colors.black,
    );
    var textStyleUrl = TextStyle(
      fontSize: 12,
      color: Colors.green,
    );

    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Column(
        children: [
          ViewHeader(),
          Text(
            "На вашем счете ${state.totals.realWallet / 100} рублей",
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          Row(
            children: [
              Checkbox(
                value: isChecked,
                onChanged: (value) {
                  setState(() {
                    isChecked = value!;
                  });
                },
              ),
              Flexible(
                child: RichText(
                  text: TextSpan(
                    style: textStyle,
                    children: [
                      TextSpan(
                        text: AppStrings.realWalletActivityPolicy1,
                      ),
                      TextSpan(
                        text: AppStrings.realWalletActivityPolicy2,
                        style: textStyleUrl,
                        recognizer: TapGestureRecognizer()
                          ..onTap = () async {
                            var url = AppStrings.urlTransferConditions;
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                      ),
                      TextSpan(
                        text: AppStrings.realWalletActivityPolicy3,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          _setMainBtns(
            label: AppStrings.btnTransferMoney,
            fun: !isChecked
                ? null
                : () {
                    BlocProvider.of<MyCashBloc>(context)
                        .add(MyCashTransferMoney());
                  },
          ),
          _setMainBtns(
            label: AppStrings.btnHistoryTransfer,
            fun: () {
              BlocProvider.of<MyCashBloc>(context).add(MyCashHistoryTransfer());
            },
          ),
        ],
      ),
    );
  }

  Widget _setStateTransferMoney(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            ViewHeader(),
            _setMainBtns(
              label: AppStrings.btnYandexCash,
              fun: () {
                BlocProvider.of<MyCashBloc>(context)
                    .add(MyCashGetTransfer(_saveChoose = 0));
              },
            ),
            _setMainBtns(
              label: AppStrings.btnPhone,
              fun: () {
                BlocProvider.of<MyCashBloc>(context)
                    .add(MyCashGetTransfer(_saveChoose = 1));
              },
            ),
          ],
        ),
      );
  }

  Widget _setStateHistoryTransfer(List<CashOutHistory> list) {
    TextStyle _textStyle = TextStyle(fontSize: 16, color: Colors.white);
    if (list.isEmpty) {
      return Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: [
              ViewHeader(),
              Padding(
                padding: EdgeInsets.only(
                  top: 8.0,
                ),
                child: Center(
                  child: Text("Нет истории переводов"),
                ),
              ),
            ],
          ),
        );
    }
    return Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            ViewHeader(),
            ListView.builder(
              itemCount: list.length,
              itemBuilder: (context, position) {
                return Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(
                      top: 4.0,
                    ),
                    child: Container(
                      color: Colors.green[800],
                      child: Column(
                        children: [
                          Text(
                            list[position].destination,
                            style: _textStyle,
                          ),
                          Text(
                            list[position].statusText,
                            style: _textStyle,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      );
  }

  Widget _setStateTransferByPhone(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            ViewHeader(),
            _setTextField(context, 1),
            _setTextFieldSum(),
            _setMainBtns(
              label: AppStrings.btnSend,
              fun: _checkPhoneOrCash && _checkSum
                  ? () {
                      BlocProvider.of<MyCashBloc>(context)
                          .add(MyCashSendMoney(YandexMoneyOut(
                        type: "tel",
                        dstAccount:
                            "+7" + _maskPhoneFormatter.getUnmaskedText(),
                        amount: _controllerSum.text,
                      )));
                    }
                  : null,
            ),
          ],
        ),
      );
  }

  Widget _setStateTransferByYandexMoney(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            ViewHeader(),
            _setTextField(context, 0),
            _setTextFieldSum(),
            _setMainBtns(
              label: AppStrings.btnSend,
              fun: _checkPhoneOrCash && _checkSum
                  ? () {
                      BlocProvider.of<MyCashBloc>(context)
                          .add(MyCashSendMoney(YandexMoneyOut(
                        type: "yandex",
                        dstAccount: _controllerPhone.text,
                        amount: _controllerSum.text,
                      )));
                    }
                  : null,
            ),
          ],
        ),
      );
  }

  Widget _setTextField(BuildContext context, int typeField) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
      child: TextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: typeField == 0
              ? AppStrings.labelFieldYandexCash
              : AppStrings.labelFieldPhoneNumber,
          suffixIcon: IconButton(
            icon: Icon(Icons.cancel_outlined),
            onPressed: () {
              _controllerPhone.clear();
            },
          ),
        ),
        controller: _controllerPhone,
        inputFormatters: typeField == 0 ? null : [_maskPhoneFormatter],
        keyboardType: TextInputType.number,
        onChanged: (value) {
          if (value.length >= 13) {
            setState(() {
              _checkPhoneOrCash = true;
            });
          } else {
            setState(() {
              _checkPhoneOrCash = false;
            });
          }
        },
      ),
    );
  }

  Widget _setTextFieldSum() {
    return TextField(
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: AppStrings.labelFieldSum,
        //hintText: AppStrings.hintFieldPhoneNumber,
        suffixIcon: IconButton(
          icon: Icon(Icons.cancel_outlined),
          onPressed: () {
            _controllerSum.clear();
          },
        ),
      ),
      controller: _controllerSum,
      keyboardType: TextInputType.number,
      onChanged: (value) {
        if (value.length > 0) {
          setState(() {
            _checkSum = true;
          });
        } else {
          setState(() {
            _checkSum = false;
          });
        }
      },
    );
  }
}
