import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nearestdistrict_app/bloc/events/find_event.dart';
import 'package:nearestdistrict_app/bloc/find_bloc.dart';
import 'package:nearestdistrict_app/bloc/states/find_state.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';
import 'package:nearestdistrict_app/utils/sp_keys.dart';
import 'package:nearestdistrict_app/view/templetes/view_header.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class ViewFind extends StatelessWidget {
  ViewFind({Key? key}) : super(key: key);

  double currentLatitude = 0;
  double currentLongitude = 0;
  late TextEditingController _controller;
  Completer<YandexMapController> _completer = Completer();

  @override
  Widget build(BuildContext context) {
    _controller = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: Text(AppStrings.titleViewFind),
      ),
      body: BlocProvider(
        create: (context) => FindBloc(),
        child: BlocBuilder<FindBloc, FindState>(
          builder: (context, state) {
            if (state is FindShowTextField) {
              return _setStateShowTextField(context);
            }
            if (state is FindNeedOrganization) {
              return _setStateFoundNeedOrganization(context, state);
            }
            return _setInitState(context);
          },
        ),
      ),
    );
  }

  //states
  Widget _setInitState(BuildContext context) {
    currentLatitude =
        inject<SharedPreferences>().getDouble(SPKeys.userCurrentPositionLat)!;
    currentLongitude =
        inject<SharedPreferences>().getDouble(SPKeys.userCurrentPositionLon)!;

    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ViewHeader(),
          Container(
            height: 112,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _setBtns(
                      AppStrings.btnFindMyScores,
                      () {},
                      1,
                    ),
                    _setBtns(
                      AppStrings.btnFindMyChoose,
                      () {},
                      2,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _setBtns(
                      AppStrings.btnFindMyStokes,
                      null,
                      1,
                    ),
                    _setBtns(
                      AppStrings.btnFindMyOrganizations,
                      () {
                        BlocProvider.of<FindBloc>(context)
                            .add(FindOrganizationBtnClick());
                      },
                      2,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              child: YandexMap(
                onMapCreated: (controller) {
                  controller.move(
                    point: Point(
                      latitude: currentLatitude,
                      longitude: currentLongitude,
                    ),
                  );
                  /*controller.addPlacemark(Placemark(
                    point: Point(
                      latitude: currentLatitude,
                      longitude: currentLongitude,
                    ),
                    style: PlacemarkStyle(
                      scale: 2.0,
                    ),
                  ));*/
                  controller.addCircle(Circle(
                    center: Point(
                      latitude: currentLatitude,
                      longitude: currentLongitude,
                    ),
                    radius: 25,
                    style: CircleStyle(
                      strokeWidth: 0,
                      fillColor: Colors.red,
                    ),
                  ));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _setStateShowTextField(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ViewHeader(),
          Container(
            height: 112,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _setBtns(
                      AppStrings.btnFindMyScores,
                      () {},
                      1,
                    ),
                    _setBtns(
                      AppStrings.btnFindMyChoose,
                      () {},
                      2,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _setBtns(
                      AppStrings.btnFindMyStokes,
                      null,
                      1,
                    ),
                    _setBtns(
                      AppStrings.btnFindMyOrganizations,
                      () {
                        BlocProvider.of<FindBloc>(context)
                            .add(FindOrganizationBtnClick());
                      },
                      2,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
            child: TextField(
              controller: _controller,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppStrings.labelFieldSearch,
                suffixIcon: IconButton(
                  icon: Icon(Icons.cancel_outlined),
                  onPressed: () {
                    _controller.clear();
                  },
                ),
              ),
              onSubmitted: (value) {
                BlocProvider.of<FindBloc>(context)
                    .add(FindOrganizationSearch(value));
              },
            ),
          ),
          Expanded(
            child: Container(
              child: YandexMap(
                onMapCreated: (controller) {
                  controller.move(
                    point: Point(
                      latitude: currentLatitude,
                      longitude: currentLongitude,
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _setStateFoundNeedOrganization(BuildContext context, FindNeedOrganization state) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ViewHeader(),
          Container(
            height: 112,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _setBtns(
                      AppStrings.btnFindMyScores,
                          () {},
                      1,
                    ),
                    _setBtns(
                      AppStrings.btnFindMyChoose,
                          () {},
                      2,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _setBtns(
                      AppStrings.btnFindMyStokes,
                      null,
                      1,
                    ),
                    _setBtns(
                      AppStrings.btnFindMyOrganizations,
                          () {
                        BlocProvider.of<FindBloc>(context)
                            .add(FindOrganizationBtnClick());
                      },
                      2,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
            child: TextField(
              controller: _controller,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppStrings.labelFieldSearch,
                suffixIcon: IconButton(
                  icon: Icon(Icons.cancel_outlined),
                  onPressed: () {
                    _controller.clear();
                  },
                ),
              ),
              onSubmitted: (value) {
                BlocProvider.of<FindBloc>(context)
                    .add(FindOrganizationSearch(value));
              },
            ),
          ),
          Expanded(
            child: Container(
              child: YandexMap(
                onMapCreated: (controller) {
                  controller.move(
                    point: Point(
                      latitude: state.organization.first.latitude!,
                      longitude: state.organization.first.longitude!,
                    ),
                  );
                  controller.addCircle(Circle(
                    center: Point(
                      latitude: state.organization.first.latitude!,
                      longitude: state.organization.first.longitude!,
                    ),
                    radius: 25,
                    style: CircleStyle(
                      strokeWidth: 0,
                      fillColor: Colors.red,
                    ),
                  ));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  //other
  Widget _setBtns(String label, void Function()? fun, int position) {
    return Expanded(
      child: GestureDetector(
        onTap: fun != null ? fun : null,
        child: Padding(
          padding: position == 1
              ? EdgeInsets.fromLTRB(0, 8, 8, 8)
              : EdgeInsets.fromLTRB(8, 8, 0, 8),
          child: Container(
            height: 30,
            decoration: BoxDecoration(
              color: fun != null ? Colors.green : Colors.grey,
              borderRadius: BorderRadius.all(Radius.circular(5)),
            ),
            child: Center(
              child: Text(
                label,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

/*void _onMapCreated(YandexMapController controller) async {
    _position = await GeoLocationService.determinePosition();
    controller.move(
        point: Point(
            latitude: _position.latitude, longitude: _position.longitude));
  }*/
}
