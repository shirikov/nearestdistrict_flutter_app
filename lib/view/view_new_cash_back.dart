import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nearestdistrict_app/bloc/events/new_cashback_event.dart';
import 'package:nearestdistrict_app/bloc/new_cashback_bloc.dart';
import 'package:nearestdistrict_app/bloc/states/new_cashback_state.dart';
import 'package:nearestdistrict_app/models/organization.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';
import 'package:nearestdistrict_app/view/templetes/view_code_cashback.dart';
import 'package:nearestdistrict_app/view/templetes/view_header.dart';

class ViewNewCashBack extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ViewNewCashBackPage();
}

class _ViewNewCashBackPage extends State<ViewNewCashBack> {
  late TextEditingController _controller;

  _ViewNewCashBackPage() {
    _controller = TextEditingController();
  }

  @override
  void initState() {
    super.initState();
    //BlocProvider.of<NewCashbackBloc>(context).add(NewCashbackLoading());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppStrings.titleViewNewCashback),
      ),
      body: BlocProvider(
        create: (context) => NewCashbackBloc(),
        child: BlocBuilder<NewCashbackBloc, NewCashbackState>(
          builder: (context, state) {
            if (state is ListOrganizationLoaded) {
              return Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    ViewHeader(),
                    Text(
                      AppStrings.newCacheBackEnterOrganisation,
                      textAlign: TextAlign.center,
                    ),
                    _setTextField(context),
                    _setList(state.list),
                  ],
                ),
              );
            }
            if (state is NewCashbackSearchComplete) {
              return Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    ViewHeader(),
                    Text(
                      AppStrings.newCacheBackEnterOrganisation,
                      textAlign: TextAlign.center,
                    ),
                    _setTextField(context),
                    _setList(state.list),
                  ],
                ),
              );
            }
            return Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: [
                  ViewHeader(),
                  Text(
                    AppStrings.newCacheBackEnterOrganisation,
                    textAlign: TextAlign.center,
                  ),
                  _setTextField(context),
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _setList(List<Organization> list) {
    return Expanded(
      child: ListView.builder(
        itemCount: list.length,
        itemBuilder: (BuildContext context, int position) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ViewCodeCashback(
                    organizationId: list[position].id,
                  ),
                ),
              );
            },
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 4.0, 0, 4.0),
              child: Container(
                height: 50,
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(16.0),
                decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.all(Radius.circular(5.0))),
                child: Text(
                  list[position].name,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _setTextField(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
      child: TextField(
        onChanged: (value) {
          BlocProvider.of<NewCashbackBloc>(context)
              .add(NewCashbackSearch(value));
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: AppStrings.labelFieldFind,
          hintText: AppStrings.hintFieldFind,
        ),
      ),
    );
  }
}
