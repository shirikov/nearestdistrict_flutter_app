import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nearestdistrict_app/bloc/events/score_event.dart';
import 'package:nearestdistrict_app/bloc/score_bloc.dart';
import 'package:nearestdistrict_app/bloc/states/score_state.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';
import 'package:nearestdistrict_app/view/templetes/view_header.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class ViewMyScore extends StatelessWidget {
  const ViewMyScore({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppStrings.titleViewMyOffers),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: BlocProvider(
          create: (context) => ScoreBloc(),
          child: BlocBuilder<ScoreBloc, ScoreState>(
            builder: (context, state) {
              if (state is ScoreShowListState) {
                return Column(
                  children: [
                    ViewHeader(),
                    Text(AppStrings.myCashBackActivitySearchLabel),
                    Expanded(
                      child: state.list.isEmpty
                          ? Center(
                              child: Text(AppStrings.emptyList),
                            )
                          : ListView.builder(
                              itemCount: state.list.length,
                              itemBuilder: (context, position) {
                                return Padding(
                                  padding: EdgeInsets.only(
                                    top: 4,
                                    bottom: 4,
                                  ),
                                );
                              },
                            ),
                    ),
                    _setMainBtns(
                      label: AppStrings.btnShowOnMap,
                      fun: () {
                        BlocProvider.of<ScoreBloc>(context).add(ScoreShowMap());
                      },
                    ),
                  ],
                );
              }
              if (state is ScoreShowMapState) {
                return Column(
                  children: [
                    ViewHeader(),
                    Text(AppStrings.myCashBackActivitySearchLabel),
                    Expanded(
                      child: YandexMap(),
                    ),
                    _setMainBtns(
                      label: AppStrings.btnShowList,
                      fun: () {
                        BlocProvider.of<ScoreBloc>(context)
                            .add(ScoreShowList());
                      },
                    ),
                  ],
                );
              }
              return Column(
                children: [
                  ViewHeader(),
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _setMainBtns({
    String label = "",
    void Function()? fun,
  }) {
    return GestureDetector(
      onTap: fun != null ? fun : null,
      child: Padding(
        padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
        child: Container(
          height: 40,
          decoration: BoxDecoration(
            color: fun != null ? Colors.green : Colors.grey,
            borderRadius: BorderRadius.all(Radius.circular(5)),
          ),
          child: Center(
            child: Text(
              label,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
