import 'dart:ui';

import 'package:flutter/material.dart';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nearestdistrict_app/bloc/events/slider_events.dart';
import 'package:nearestdistrict_app/bloc/slider_bloc.dart';
import 'package:nearestdistrict_app/bloc/states/slider_state.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';
import 'package:nearestdistrict_app/utils/sp_keys.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ViewSlider extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ViewSliderPage();
}

class _ViewSliderPage extends State<ViewSlider> {
  final List<String> urlAssets = [
    "assets/sliders/slide1.png",
    "assets/sliders/slide2.png",
    "assets/sliders/slide3.png",
    "assets/sliders/slide4.png",
    "assets/sliders/slide5.png",
  ];
  final List<String> imgList = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80'
  ];

  final List<Widget> _listSvg = [];
  CarouselController _controller = CarouselController();
  int _current = 0;

  @override
  void initState() {
    super.initState();
    for (String url in urlAssets) {
      _listSvg.add(Image.asset(url));
    }
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height - 200;
    return BlocProvider(
      create: (context) => SliderBloc(),
      child: BlocBuilder<SliderBloc, SliderState>(
        builder: (context, state) {
          if (state is SliderStateEnd) {
            return Padding(
              padding: EdgeInsets.fromLTRB(0.0, 0, 0.0, 0),
              child: Container(
                color: Colors.green[800],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: _setCarousel(context)..add(_setBtn()),
                ),
              ),
            );
          }
          return Padding(
            padding: EdgeInsets.fromLTRB(0.0, 0, 0.0, 0),
            child: Container(
              color: Colors.green[800],
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: _setCarousel(context),
              ),
            ),
          );
        },
      ),
    );
  }

  List<Widget> _setCarousel(BuildContext context) {
    final double height = MediaQuery.of(context).size.height - 200;
    return [
      CarouselSlider.builder(
        itemCount: urlAssets.length,
        carouselController: _controller,
        itemBuilder: (context, itemIndex, pageViewIndex) {
          return _listSvg[itemIndex];
        },
        options: CarouselOptions(
          viewportFraction: 1,
          enlargeCenterPage: true,
          enableInfiniteScroll: false,
          aspectRatio: 2.0,
          height: height,
          onPageChanged: (index, reason) {
            setState(() {
              _current = index;
            });
            if (index == urlAssets.length - 1)
              BlocProvider.of<SliderBloc>(context).add(SliderEventEnd());
          },
        ),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: imgList.asMap().entries.map((entry) {
          return GestureDetector(
            onTap: () => _controller.animateToPage(entry.key),
            child: Container(
              width: 12.0,
              height: 12.0,
              margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color:
                      (/*Theme.of(context).brightness == Brightness.dark
                              ? */
                              Colors.white
                          /*: Colors.black*/)
                          .withOpacity(_current == entry.key ? 0.9 : 0.4)),
            ),
          );
        }).toList(),
      ),
    ];
  }

  Widget _setBtn() {
    return GestureDetector(
      onTap: () {
        inject<SharedPreferences>().setBool(SPKeys.needIntro, false);
        Navigator.of(context).pushNamed('login');
      },
      child: Padding(
        padding: EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
        child: Container(
          height: 40,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(5)),
          ),
          child: Center(
            child: Text(
              AppStrings.btnStart,
              style: TextStyle(
                color: Colors.green,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
