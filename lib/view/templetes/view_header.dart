import 'package:flutter/material.dart';
import 'package:flutter_svg/avd.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';

class ViewHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.only(right: 16.0, bottom: 8.0),
          child: Container(
            width: 50,
            height: 50,
            child: AvdPicture.asset('assets/drawable/app_logo_new.xml'),
          ),
        ),
        Text(AppStrings.leftTitle, style: TextStyle( color: Colors.green[800], fontSize: 18),),
        Text(AppStrings.rightTitle, style: TextStyle( color: Colors.orange[800], fontSize: 18),),
      ],
    );
  }
}
