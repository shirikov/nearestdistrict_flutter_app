import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nearestdistrict_app/bloc/code_bloc.dart';
import 'package:nearestdistrict_app/bloc/events/code_event.dart';
import 'package:nearestdistrict_app/bloc/states/code_state.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';
import 'package:nearestdistrict_app/view/templetes/view_header.dart';

class ViewCodeCashback extends StatefulWidget {
  final String organizationId;

  const ViewCodeCashback({Key? key, required this.organizationId})
      : super(key: key);

  @override
  _ViewCodeCashbackState createState() => _ViewCodeCashbackState();
}

class _ViewCodeCashbackState extends State<ViewCodeCashback> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppStrings.titleViewNewCashback),
      ),
      body: BlocProvider(
        create: (context) => CodeBloc(widget.organizationId),
        child: BlocBuilder<CodeBloc, CodeState>(
          builder: (context, state) {
            if (state is CodeReceived) {
              return Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    ViewHeader(),
                    _setTextView(state.code),
                    GestureDetector(
                      onTap: () {
                        BlocProvider.of<CodeBloc>(context)
                            .add(CompleteUseCodePromo());
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                          top: 16.0,
                        ),
                        alignment: Alignment.center,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Colors.green[800],
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                        child: Text(
                          AppStrings.btnReady,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }
            if (state is CompleteUseCodePromoState) {
              return Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    ViewHeader(),
                    Text(
                      AppStrings.newCacheBackDoneScore,
                      textAlign: TextAlign.center,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pushNamed('main');
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                          top: 16.0,
                        ),
                        alignment: Alignment.center,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Colors.green[800],
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                        child: Text(
                          AppStrings.btnOk,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }
            return Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: [
                  ViewHeader(),
                  Center(
                    child: CircularProgressIndicator(),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _setTextView(String code) {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        style: TextStyle(
          color: Colors.black,
        ),
        children: [
          TextSpan(
            text: AppStrings.newCacheBackIDLabel,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          TextSpan(
            text: _convertCode(code),
            style: TextStyle(
              fontSize: 56,
              fontWeight: FontWeight.bold,
              letterSpacing: 2,
            ),
          ),
          TextSpan(
            text: AppStrings.newCacheBackIDLabelSecond,
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          TextSpan(
            text: AppStrings.newCacheBackIDLabelThird,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }

  String _convertCode(String code) {
    return code.substring(0, 2) +
        "-" +
        code.substring(2, 4) +
        "-" +
        code.substring(4) +
        "\n";
  }
}
