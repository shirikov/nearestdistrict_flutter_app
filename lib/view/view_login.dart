import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:nearestdistrict_app/bloc/events/login_events.dart';
import 'package:nearestdistrict_app/bloc/login_bloc.dart';
import 'package:nearestdistrict_app/bloc/states/login_state.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';
import 'package:nearestdistrict_app/view/templetes/view_header.dart';
import 'package:url_launcher/url_launcher.dart';

class ViewLogin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ViewLoginPage();
}

class _ViewLoginPage extends State<ViewLogin> {
  late TextEditingController _controllerPhone;
  late TextEditingController _controllerName;
  late TextEditingController _controllerPromo;
  late TextEditingController _controllerCode;
  bool isChecked = false;

  var maskPhoneFormatter = MaskTextInputFormatter(
    mask: '+7 (###) ### ## ##',
    filter: {'#': RegExp(r'[0-9]')},
  );

  @override
  void initState() {
    super.initState();
    _controllerPhone = TextEditingController();
    _controllerName = TextEditingController();
    _controllerPromo = TextEditingController();
    _controllerCode = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _setAppBar(),
      body: BlocProvider(
        create: (context) => LoginBloc(),
        child: BlocBuilder<LoginBloc, LoginState>(
          builder: (context, state) {
            if (state is ShowCodeField) {
              return Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      ViewHeader(),
                      _setTextFieldPhone(context),
                      _setPolicyField(),
                      _setMainBtns(
                        fun: !isChecked
                            ? null
                            : () {
                                BlocProvider.of<LoginBloc>(context)
                                    .add(LoginClientGetSmsCode(
                                  phone: '+7' +
                                      maskPhoneFormatter.getUnmaskedText(),
                                  stateAuth: 'login',
                                ));
                              },
                        label: AppStrings.btnLoginGetCode,
                      ),
                    ],
                  ),
                );
            }
            if (state is LoginClientGetSmsCodeLog) {
              return Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      ViewHeader(),
                      _setTextFieldPhone(context),
                      _setPolicyField(),
                      _setCodeField(),
                      _setMainBtns(
                        fun: !isChecked
                            ? null
                            : () {
                                BlocProvider.of<LoginBloc>(context)
                                    .add(LoginClientLogin(
                                  phone: '+7' +
                                      maskPhoneFormatter.getUnmaskedText(),
                                  smsCode: _controllerCode.text,
                                ));
                              },
                        label: AppStrings.btnLoginSignIn,
                      ),
                    ],
                  ),
                );
            }
            if (state is ShowName) {
              return Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    ViewHeader(),
                    _setTextFieldPhone(context),
                    _setFieldForReg(
                      _controllerName,
                      AppStrings.labelFieldName,
                      AppStrings.hintFieldName,
                    ),
                    Text(AppStrings.loginActivityLoginLabel),
                    _setFieldForReg(
                      _controllerPromo,
                      AppStrings.labelFieldPromoCode,
                      AppStrings.hintFieldPromoCode,
                    ),
                    _setPolicyField(),
                    _setMainBtns(
                      fun: !isChecked
                          ? null
                          : () {
                              BlocProvider.of<LoginBloc>(context)
                                  .add(LoginClientGetSmsCode(
                                phone:
                                    '+7' + maskPhoneFormatter.getUnmaskedText(),
                                stateAuth: 'reg',
                              ));
                            },
                      label: AppStrings.btnLoginGetCode,
                    ),
                  ],
                ),
              );
            }
            if (state is LoginClientGetSmsCodeReg) {
              return Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    ViewHeader(),
                    _setTextFieldPhone(context),
                    _setFieldForReg(
                      _controllerName,
                      AppStrings.labelFieldName,
                      AppStrings.hintFieldName,
                    ),
                    Text(AppStrings.loginActivityLoginLabel),
                    _setFieldForReg(
                      _controllerPromo,
                      AppStrings.labelFieldPromoCode,
                      AppStrings.hintFieldPromoCode,
                    ),
                    _setPolicyField(),
                    _setCodeField(),
                    _setMainBtns(
                      fun: !isChecked
                          ? null
                          : () {
                              BlocProvider.of<LoginBloc>(context)
                                  .add(LoginClientRegistration(
                                firstName: _controllerName.text,
                                lastName: '',
                                phone:
                                    '+7' + maskPhoneFormatter.getUnmaskedText(),
                                smsCode: _controllerCode.text,
                              ));
                            },
                      label: AppStrings.btnLoginSignUp,
                    ),
                  ],
                ),
              );
            }
            if (state is LoginClientLoginSuccess) {
              Future.delayed(Duration.zero, () {
                Navigator.of(context).pushNamed('main');
              });
            }
            return Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: [
                  ViewHeader(),
                  _setTextFieldPhone(context),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  AppBar _setAppBar() {
    return AppBar(
      title: Text(AppStrings.titleViewLogin),
    );
  }

  Widget _setTextFieldPhone(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
      child: TextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: AppStrings.labelFieldPhoneNumber,
          suffixIcon: IconButton(
            icon: Icon(Icons.cancel_outlined),
            onPressed: () {
              _controllerPhone.clear();
            },
          ),
        ),
        controller: _controllerPhone,
        inputFormatters: [maskPhoneFormatter],
        keyboardType: TextInputType.number,
        onChanged: (String value) {
          if (value.length == 18) {
            print('+7' + maskPhoneFormatter.getUnmaskedText());
            BlocProvider.of<LoginBloc>(context).add(LoginCheckPhone(
              phone: '+7' + maskPhoneFormatter.getUnmaskedText(),
            ));
          }
        },
      ),
    );
  }

  Widget _setPolicyField() {
    var textStyle = TextStyle(
      fontSize: 14,
      color: Colors.black,
    );
    var textStyleUrl = TextStyle(
      fontSize: 14,
      color: Colors.green,
    );
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
      child: Row(
        children: [
          Checkbox(
            value: isChecked,
            onChanged: (value) {
              setState(() {
                isChecked = value!;
              });
            },
          ),
          Flexible(
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: AppStrings.policyBigShow1,
                    style: textStyle,
                  ),
                  TextSpan(
                    text: AppStrings.privacyPolicy,
                    style: textStyleUrl,
                    recognizer: TapGestureRecognizer()
                      ..onTap = () async {
                        var url = AppStrings.urlPrivacyPolicy;
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                  ),
                  TextSpan(
                    text: AppStrings.policyBigShow2,
                    style: textStyle,
                  ),
                  TextSpan(
                    text: AppStrings.userAgreement,
                    style: textStyleUrl,
                    recognizer: TapGestureRecognizer()
                      ..onTap = () async {
                        var url = AppStrings.urlUserAgreement;
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _setCodeField() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
      child: TextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: AppStrings.labelFieldCode,
        ),
        controller: _controllerCode,
        keyboardType: TextInputType.number,
      ),
    );
  }

  Widget _setFieldForReg(
      TextEditingController controller, String label, String hint) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
      child: TextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: label,
          hintText: hint,
        ),
        controller: controller,
      ),
    );
  }

  Widget _setMainBtns({
    String label = "",
    void Function()? fun,
  }) {
    return GestureDetector(
      onTap: fun != null ? fun : null,
      child: Padding(
        padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
        child: Container(
          height: 40,
          decoration: BoxDecoration(
            color: fun != null ? Colors.green : Colors.grey,
            borderRadius: BorderRadius.all(Radius.circular(5)),
          ),
          child: Center(
            child: Text(
              label,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
