import 'package:bloc/bloc.dart';
import 'package:nearestdistrict_app/bloc/events/new_cashback_event.dart';
import 'package:nearestdistrict_app/bloc/states/new_cashback_state.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/models/organization.dart';
import 'package:nearestdistrict_app/service/api_service.dart';
import 'package:nearestdistrict_app/service/geo_location_service.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';

class NewCashbackBloc extends Bloc<NewCashbackEvent, NewCashbackState> {
  NewCashbackBloc() : super(NewCashbackState()) {
    add(NewCashbackLoading());
  }

  List<Organization> _list = [];

  @override
  Stream<NewCashbackState> mapEventToState(NewCashbackEvent event) async* {
    if (event is NewCashbackLoading) {
      yield* _loadData();
    }
    if (event is NewCashbackSearch) {
      yield* _searchCashback(event);
    }
  }

  Stream<NewCashbackState> _loadData() async* {
    yield LoadingProgress();
    try {
      var location = await GeoLocationService.determinePosition();
      var data = {
        'latitude': location.latitude,
        'longitude': location.longitude,
      };
      var res = await inject<ApiService>().getCacheBackByCoordinates(data);
      if (res.code == AppStrings.codeSuccess) {
        _list = res.data;
        yield ListOrganizationLoaded(res.data);
      }
    } catch (e) {
      print(e.toString());
      yield NewCashbackFailure();
    }
  }

  Stream<NewCashbackState> _searchCashback(NewCashbackSearch event) async* {
    yield LoadingProgress();
    try {
      yield NewCashbackSearchComplete(_list
          .where((element) => element.name
              .toLowerCase()
              .contains(event.valueSearch.toLowerCase()))
          .toList());
    } catch (e) {
      print(e.toString());
      yield NewCashbackFailure();
    }
  }
}
