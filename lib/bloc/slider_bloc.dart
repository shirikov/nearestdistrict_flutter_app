import 'package:bloc/bloc.dart';
import 'package:nearestdistrict_app/bloc/events/slider_events.dart';
import 'package:nearestdistrict_app/bloc/states/slider_state.dart';

class SliderBloc extends Bloc<SliderEvents, SliderState> {
  SliderBloc() : super(SliderState());

  @override
  Stream<SliderState> mapEventToState(SliderEvents event) async*{
    if (event is SliderEventEnd) {
      yield SliderStateEnd();
    }
  }
}