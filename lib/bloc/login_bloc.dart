import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:nearestdistrict_app/bloc/events/login_events.dart';
import 'package:nearestdistrict_app/bloc/states/login_state.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/models/login_data.dart';
import 'package:nearestdistrict_app/models/response_header.dart';
import 'package:nearestdistrict_app/service/api_service.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';
import 'package:nearestdistrict_app/utils/sp_keys.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginBloc extends Bloc<LoginEvents, LoginState> {
  String _userUid = "";
  String _verificationId = "";
  int? _resendToken = 0;

  LoginBloc() : super(LoginState());

  @override
  Stream<LoginState> mapEventToState(LoginEvents event) async* {
    if (event is LoginCheckPhone) {
      yield* _requestCheckPhone(event);
    }
    if (event is LoginClientGetSmsCode) {
      yield* _requestSmsCode(event);
    }
    if (event is LoginClientLogin) {
      yield* _requestClientLogin(event);
    }
    if (event is LoginClientRegistration) {
      yield* _requestClientRegistration(event);
    }
  }

  Stream<LoginState> _requestCheckPhone(LoginCheckPhone event) async* {
    //yield LoadingInProgress();
    try {
      Map<String, String> data = {'phone': event.phone};
      var res = await inject<ApiService>().phoneCheck(data);
      if (res.data.isPresent()) {
        yield ShowCodeField();
      } else {
        yield ShowName();
      }
    } catch (e) {
      print(e.toString());
      yield LoginFailure();
    }
  }

  Stream<LoginState> _requestSmsCode(LoginClientGetSmsCode event) async* {
    //yield LoadingInProgress();
    try {
      print(event.phone);
      if (event.stateAuth == 'login') {
        yield LoginClientGetSmsCodeLog();
      } else if (event.stateAuth == 'reg') {
        yield LoginClientGetSmsCodeReg();
      }
      await inject<FirebaseAuth>().verifyPhoneNumber(
        phoneNumber: event.phone,
        verificationCompleted: (PhoneAuthCredential credential) {
          _verificationCompleted(credential);
        },
        verificationFailed: (FirebaseAuthException e) {
          _verificationFailed(e);
        },
        codeSent: (String verificationId, int? resendToken) {
          _codeSent(verificationId, resendToken);
        },
        codeAutoRetrievalTimeout: (String verificationId) {
          _codeAutoRetrievalTimeout(verificationId);
        },
      ).catchError((onError) => {
        print(onError),
      });
      //_firebaseAuth.signInWithPhoneNumber(event.phone);
    } catch (e) {
      print(e.toString());
      yield LoginFailure();
    }
  }

  Stream<LoginState> _requestClientLogin(LoginClientLogin event) async* {
    try {
      if (_userUid == "") {
        PhoneAuthCredential credential = PhoneAuthProvider.credential(
          verificationId: _verificationId,
          smsCode: event.smsCode,
        );
        var userCredential = await inject<FirebaseAuth>().signInWithCredential(credential);
        _userUid = userCredential.user!.uid;
        await inject<FirebaseAuth>().signOut();
      }
      if (await _login(event) == AppStrings.codeSuccess) {
        yield LoginClientLoginSuccess();
      } else {
        yield LoginFailure();
      }
    } catch (e) {
      print(e.toString());
      yield LoginFailure();
    }
  }

  Stream<LoginState> _requestClientRegistration(LoginClientRegistration event) async* {
    try {
      if (_userUid == "") {
        PhoneAuthCredential credential = PhoneAuthProvider.credential(
          verificationId: _verificationId,
          smsCode: event.smsCode,
        );
        var userCredential = await inject<FirebaseAuth>().signInWithCredential(credential);
        _userUid = userCredential.user!.uid;
        await inject<FirebaseAuth>().signOut();
      }
      if (await _registration(event) == AppStrings.codeSuccess) {
        yield LoginClientLoginSuccess();
      } else {
        yield LoginFailure();
      }
    } catch(e) {
      print(e.toString());
      yield LoginFailure();
    }
  }

  Future<String> _login(LoginClientLogin event) async {
    try {
      var data = {'phone': event.phone, 'uid': _userUid};
      var res = await inject<ApiService>().clientLogin(data);
      _saveData(res);
      return res.code;
    } catch(e) {
      print(e.toString());
      return '';
    }
  }

  Future<String> _registration(LoginClientRegistration event) async {
    try {
      var data = {
        'firstName': event.firstName,
        'lastName': event.lastName,
        'phone': event.phone,
        'uid': _userUid,
      };
      var res = await inject<ApiService>().clientRegistration(data);
      _saveData(res);
      return res.code;
    } catch(e) {
      print(e.toString());
      return '';
    }
  }

  void _saveData(ResponseHeader<LoginData> res) async {
    await inject<SharedPreferences>().setString(SPKeys.userInfo, jsonEncode(res.data.user.toJson()));
    await inject<SharedPreferences>().setString(SPKeys.userToken, res.data.token);
    await inject<SharedPreferences>().setBool(SPKeys.userLogged, true);
    updateRetrofit();
  }
  //firebase auth
  void _verificationCompleted(PhoneAuthCredential pac) async {
    try {
      var userCredential = await inject<FirebaseAuth>().signInWithCredential(pac);
      _userUid = userCredential.user!.uid;
      print(_userUid);
      await inject<FirebaseAuth>().signOut();
    } catch (e) {
      print(e.toString());
    }
  }

  void _verificationFailed(FirebaseAuthException e) {
    print(e.message);
  }

  void _codeSent(String verificationId, int? resendToken) {
    try {
      _verificationId = verificationId;
      _resendToken = resendToken;
      print(verificationId + " : " + resendToken.toString());
    } catch (e) {
      print(e.toString());
    }
  }

  void _codeAutoRetrievalTimeout(String verificationId) {}
}
