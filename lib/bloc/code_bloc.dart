import 'package:bloc/bloc.dart';
import 'package:nearestdistrict_app/bloc/events/code_event.dart';
import 'package:nearestdistrict_app/bloc/states/code_state.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/models/promo_list.dart';
import 'package:nearestdistrict_app/models/response_header.dart';
import 'package:nearestdistrict_app/service/api_service.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';

class CodeBloc extends Bloc<CodeEvent, CodeState> {
  String _organizationId;
  CodeBloc(this._organizationId) : super(CodeState()) {
    add(GetCodeEvent(_organizationId));
  }

  ResponseHeader<PromoList>? _result;

  @override
  Stream<CodeState> mapEventToState(CodeEvent event) async* {
    if (event is GetCodeEvent) {
      yield* _getCode(event);
    }
    if (event is CompleteUseCodePromo) {
      yield CompleteUseCodePromoState();
    }
  }

  Stream<CodeState> _getCode(GetCodeEvent event) async* {
    /*try {*/
      var data = {'organizationId': event.organizationId};
      var res = await inject<ApiService>().getCacheBackPromo(data);
      _result = res;
      if (res.code == AppStrings.codeSuccess) {
        yield CodeReceived(res.data.code);
      } else {
        yield CodeFailure();
      }
    /*} catch(e) {
      print(e.toString());
      yield CodeFailure();
    }*/
  }
}