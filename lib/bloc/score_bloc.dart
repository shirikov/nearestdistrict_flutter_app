import 'package:bloc/bloc.dart';
import 'package:nearestdistrict_app/bloc/events/score_event.dart';
import 'package:nearestdistrict_app/bloc/states/score_state.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/service/api_service.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';

class ScoreBloc extends Bloc<ScoreEvent, ScoreState> {
  ScoreBloc() : super(ScoreState()) {
    add(ScoreShowList());
  }

  @override
  Stream<ScoreState> mapEventToState(ScoreEvent event) async*{
    if (event is ScoreShowList) {
      yield* _initShow();
    }
    if (event is ScoreShowMap) {
      yield ScoreShowMapState();
    }
  }

  Stream<ScoreState> _initShow() async* {
    try {
      var res = await inject<ApiService>().getMyCash();
      if (res.code == AppStrings.codeSuccess) {
        yield ScoreShowListState(res.data);
      } else {
        yield ScoreFailure();
      }
    } catch(e) {
      print(e.toString());
      yield ScoreFailure();
    }
  }

}