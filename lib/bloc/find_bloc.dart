import 'package:bloc/bloc.dart';
import 'package:nearestdistrict_app/bloc/events/find_event.dart';
import 'package:nearestdistrict_app/bloc/states/find_state.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/service/api_service.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';

class FindBloc extends Bloc<FindEvent, FindState> {
  FindBloc() : super(FindState());

  @override
  Stream<FindState> mapEventToState(FindEvent event) async* {
    if (event is FindOrganizationBtnClick) {
      yield FindShowTextField();
    }
    if (event is FindOrganizationSearch) {
      yield* _getOrganizationByName(event);
    }
  }

  Stream<FindState> _getOrganizationByName(FindOrganizationSearch event) async* {
    try {
      var data = {'search' : event.organizationName};
      var res = await inject<ApiService>().getCacheBackByName(data);
      if (res.code == AppStrings.codeSuccess) {
        yield FindNeedOrganization(res.data);
      } else {
        yield FindFailure();
      }
    } catch(e) {
      print(e.toString());
      yield FindFailure();
    }
  }

}