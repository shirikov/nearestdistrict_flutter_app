import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:nearestdistrict_app/bloc/events/main_events.dart';
import 'package:nearestdistrict_app/bloc/states/main_state.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/models/response_header.dart';
import 'package:nearestdistrict_app/models/totals.dart';
import 'package:nearestdistrict_app/service/api_service.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';
import 'package:nearestdistrict_app/utils/sp_keys.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainBloc extends Bloc<MainEvents, MainState> {

  MainBloc() : super(MainState()) {
    add(MainGetTotals());
  }

  ResponseHeader<Totals>? _result;

  @override
  Stream<MainState> mapEventToState(MainEvents event) async* {
    if (event is MainGetTotals) {
      yield* _mainInit(event);
    }
  }

  Stream<MainState> _mainInit(MainGetTotals event) async* {
    try {
      var res = _result = await inject<ApiService>().getTotals();
      inject<SharedPreferences>().setString(SPKeys.userTotals, jsonEncode(res.data.toJson()));
      if (res.code == AppStrings.codeSuccess) {
        yield MainSetTotal(res.data);
      }
    } catch(e) {
      print(e.toString());
      yield MainFailure();
    }
  }
}