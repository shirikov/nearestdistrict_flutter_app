import 'package:equatable/equatable.dart';
import 'package:nearestdistrict_app/models/yandex_money_out.dart';

class MyCashEvent extends Equatable {
  @override
  List<Object?> get props => [];

}

class MyCashInit extends MyCashEvent {}

class MyCashTransferMoney extends MyCashEvent {}

class MyCashHistoryTransfer extends MyCashEvent {}

class MyCashGetTransfer extends MyCashEvent {
  final int selectionMethod;

  MyCashGetTransfer(this.selectionMethod);
}

class MyCashGetHistoryTransfer extends MyCashEvent {}

class MyCashSendMoney extends MyCashEvent {
  final YandexMoneyOut yandexMoneyOut;

  MyCashSendMoney(this.yandexMoneyOut);
}