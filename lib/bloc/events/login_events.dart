import 'package:equatable/equatable.dart';

abstract class LoginEvents extends Equatable {
  const LoginEvents([List props = const []]);
}

class LoginCheckPhone extends LoginEvents {
  final String phone;

  LoginCheckPhone({this.phone = ''});

  @override
  List<Object?> get props => [phone];
}

class LoginClientGetSmsCode extends LoginEvents {
  final String phone;
  final String stateAuth;

  LoginClientGetSmsCode({
    required this.phone,
    required this.stateAuth,
  });

  @override
  List<Object?> get props => [];
}

class LoginClientLogin extends LoginEvents {
  final String phone;
  final String smsCode;

  LoginClientLogin({
    required this.phone,
    required this.smsCode,
  });

  @override
  List<Object?> get props => [phone, smsCode];
}

class LoginClientRegistration extends LoginEvents {
  final String phone;
  final String firstName;
  final String lastName;
  final String smsCode;

  LoginClientRegistration({
    required this.phone,
    required this.firstName,
    required this.lastName,
    required this.smsCode,
  });

  @override
  List<Object?> get props => [phone, firstName, lastName, smsCode];
}
