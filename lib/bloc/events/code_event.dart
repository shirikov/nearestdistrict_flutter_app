import 'package:equatable/equatable.dart';

class CodeEvent extends Equatable {

  const CodeEvent();

  @override
  List<Object?> get props => [];
}

class GetCodeEvent extends CodeEvent {
  final String organizationId;

  const GetCodeEvent(this.organizationId);
}

class CompleteUseCodePromo extends CodeEvent {}