import 'package:equatable/equatable.dart';

class NewCashbackEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class NewCashbackLoading extends NewCashbackEvent {}

class NewCashbackSearch extends NewCashbackEvent {
  final String valueSearch;

  NewCashbackSearch(this.valueSearch);
}