import 'package:equatable/equatable.dart';

abstract class MainEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class MainGetTotals extends MainEvents {

}