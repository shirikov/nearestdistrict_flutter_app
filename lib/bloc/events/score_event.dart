import 'package:equatable/equatable.dart';

class ScoreEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class ScoreShowList extends ScoreEvent {}

class ScoreShowMap extends ScoreEvent {}