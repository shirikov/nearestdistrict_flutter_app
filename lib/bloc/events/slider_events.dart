import 'package:equatable/equatable.dart';

class SliderEvents extends Equatable {
  @override
  List<Object?> get props => [];
}

class SliderEventEnd extends SliderEvents {}