import 'package:equatable/equatable.dart';

class OffersEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class OffersShowMap extends OffersEvent {}

class OffersShowList extends OffersEvent {}