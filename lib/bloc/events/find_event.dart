import 'package:equatable/equatable.dart';

class FindEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class FindOrganizationBtnClick extends FindEvent {}

class FindOrganizationSearch extends FindEvent {
  String organizationName = "";

  FindOrganizationSearch(this.organizationName);

  @override
  List<Object?> get props => [organizationName];
}