import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:nearestdistrict_app/bloc/events/my_cash_event.dart';
import 'package:nearestdistrict_app/bloc/states/my_cash_state.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/models/totals.dart';
import 'package:nearestdistrict_app/service/api_service.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';
import 'package:nearestdistrict_app/utils/sp_keys.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyCashBloc extends Bloc<MyCashEvent, MyCashState> {

  Totals? _totals;

  MyCashBloc() : super(MyCashState()){
    _totals = Totals.fromJson(jsonDecode(inject<SharedPreferences>().getString(SPKeys.userTotals)!));
    add(MyCashInit());
  }

  @override
  Stream<MyCashState> mapEventToState(MyCashEvent event) async* {
    if (event is MyCashInit) {
      yield* _init();
    }
    if (event is MyCashTransferMoney) {
      yield MyCashTransfer();
    }
    if (event is MyCashGetTransfer) {
      yield MyCashTransferChoose(event.selectionMethod, _totals!.realWallet / 100);
    }
    if (event is MyCashHistoryTransfer) {
      yield* _getHistoryTransfer();
    }
    if (event is MyCashSendMoney) {
      yield* _sendMoney(event);
    }
  }

  Stream<MyCashState> _init() async*{
    try {
      yield MyCashInitState(_totals!);
    }catch(e) {
      print(e.toString());
      yield MyCashFailure();
    }
  }

  Stream<MyCashState> _getHistoryTransfer() async* {
    try {
      var res = await inject<ApiService>().getCashOutHistory();
      if (res.code == AppStrings.codeSuccess) {
        yield MyCashHistoryTransferReceived(res.data);
      } else {
        yield MyCashFailure();
      }
    } catch(e) {
      print(e.toString());
      yield MyCashFailure();
    }
  }

  Stream<MyCashState> _sendMoney(MyCashSendMoney event) async*{
    try {
      print(event.yandexMoneyOut.amount);
      var res = await inject<ApiService>().makeMoneyOut(event.yandexMoneyOut);
      if (res.code == AppStrings.codeSuccess) {

      }
    } catch(e) {
      print(e.toString());
      yield MyCashFailure(errorMessage: (e as DioError).response!.data['error']);
    }
  }
}