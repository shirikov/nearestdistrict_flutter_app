import 'package:bloc/bloc.dart';
import 'package:nearestdistrict_app/bloc/events/offers_event.dart';
import 'package:nearestdistrict_app/bloc/states/offers_state.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/service/api_service.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';

class OffersBloc extends Bloc<OffersEvent, OffersState> {
  OffersBloc() : super(OffersState()) {
    add(OffersShowList());
  }

  @override
  Stream<OffersState> mapEventToState(OffersEvent event) async* {
    if (event is OffersShowMap) {
      yield OffersShowMapState();
    }
    if (event is OffersShowList) {
      yield* _showList();
    }
  }

  Stream<OffersState> _showList() async*{
    try {
      var res = await inject<ApiService>().getMyOffers();
      if (res.code == AppStrings.codeSuccess) {
        yield OffersShowListState(res.data);
      } else {
        yield OffersFailure();
      }
    } catch(e) {
      print(e.toString());
      yield OffersFailure();
    }
  }
}