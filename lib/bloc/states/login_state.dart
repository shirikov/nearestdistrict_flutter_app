import 'package:equatable/equatable.dart';
import 'package:nearestdistrict_app/models/response_header.dart';
import 'package:nearestdistrict_app/models/roles.dart';

class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object?> get props => [];
}

class LoginCheckPhoneSuccess extends LoginState {
  final ResponseHeader<Roles> requestResult;

  const LoginCheckPhoneSuccess(this.requestResult);
}

class LoginClientGetSmsCodeLog extends LoginState {}
class LoginClientGetSmsCodeReg extends LoginState {}

class LoginClientLoginSuccess extends LoginState {}

class LoginFailure extends LoginState {}

class LoadingInProgress extends LoginState {}
class ShowName extends LoginState {}
class ShowCodeField extends LoginState {}
