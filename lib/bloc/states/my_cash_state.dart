import 'package:equatable/equatable.dart';
import 'package:nearestdistrict_app/models/cash_out_history.dart';
import 'package:nearestdistrict_app/models/totals.dart';

class MyCashState extends Equatable {
  @override
  List<Object?> get props => [];

}

class MyCashInitState extends MyCashState {
  final Totals totals;

  MyCashInitState(this.totals);
}

class MyCashTransfer extends MyCashState {}

class MyCashTransferChoose extends MyCashState {
  final int selectionMethod;
  final double realWallet;
  MyCashTransferChoose(this.selectionMethod, this.realWallet);
}

class MyCashHistoryTransferReceived extends MyCashState {
  final List<CashOutHistory> list;
  MyCashHistoryTransferReceived(this.list);
}

class MyCashMoneySendSuccess extends MyCashState {
  final String status;

  MyCashMoneySendSuccess(this.status);
}

class MyCashFailure extends MyCashState {
  final String errorMessage;

  MyCashFailure({this.errorMessage = ""});
}