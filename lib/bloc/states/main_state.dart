import 'package:equatable/equatable.dart';
import 'package:nearestdistrict_app/models/totals.dart';

class MainState extends Equatable {
  @override
  List<Object?> get props => [];
}

class MainSetTotal extends MainState {
  final Totals totals;

  MainSetTotal(this.totals);
}

class MainFailure extends MainState {}