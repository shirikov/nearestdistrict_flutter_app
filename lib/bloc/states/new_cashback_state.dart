import 'package:equatable/equatable.dart';
import 'package:nearestdistrict_app/models/organization.dart';

class NewCashbackState extends Equatable {
  const NewCashbackState();

  @override
  List<Object?> get props => [];
}

class ListOrganizationLoaded extends NewCashbackState {
  final List<Organization> list;

  const ListOrganizationLoaded(this.list);
}

class NewCashbackSearchComplete extends NewCashbackState {
  final List<Organization> list;

  const NewCashbackSearchComplete(this.list);
}

class LoadingProgress extends NewCashbackState {}
class NewCashbackFailure extends NewCashbackState {}