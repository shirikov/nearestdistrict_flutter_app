import 'package:equatable/equatable.dart';
import 'package:nearestdistrict_app/models/organization.dart';

class FindState extends Equatable {
  @override
  List<Object?> get props => [];
}

class FindShowTextField extends FindState {}

class FindNeedOrganization extends FindState {
  List<Organization> organization;

  FindNeedOrganization(this.organization);

  @override
  List<Object?> get props => [organization];
}


class FindFailure extends FindState {}