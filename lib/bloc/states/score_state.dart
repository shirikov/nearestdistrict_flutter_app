import 'package:equatable/equatable.dart';
import 'package:nearestdistrict_app/models/cash_book.dart';

class ScoreState extends Equatable {
  @override
  List<Object?> get props => [];
}

class ScoreShowListState extends ScoreState {
  final List<CashBook> list;

  ScoreShowListState(this.list);
}

class ScoreShowMapState extends ScoreState {}

class ScoreFailure extends ScoreState {}