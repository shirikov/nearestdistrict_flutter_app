import 'package:equatable/equatable.dart';

class SliderState extends Equatable {
  @override
  List<Object?> get props => [];
}

class SliderStateEnd extends SliderState {}