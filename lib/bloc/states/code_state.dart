import 'package:equatable/equatable.dart';

class CodeState extends Equatable {

  const CodeState();

  @override
  List<Object?> get props => [];
}

class CodeReceived extends CodeState {
  final String code;

  const CodeReceived(this.code);
}

class CompleteUseCodePromoState extends CodeState {}

class CodeFailure extends CodeState {}