import 'package:equatable/equatable.dart';
import 'package:nearestdistrict_app/models/big_offer.dart';

class OffersState extends Equatable {
  @override
  List<Object?> get props => [];
}

class OffersShowMapState extends OffersState {}

class OffersShowListState extends OffersState {
  final List<BigOffer> list;

  OffersShowListState(this.list);
}

class OffersFailure extends OffersState {}