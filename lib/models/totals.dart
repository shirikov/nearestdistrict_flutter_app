import 'package:nearestdistrict_app/models/big_category.dart';
import 'package:nearestdistrict_app/models/category.dart';
import 'package:json_annotation/json_annotation.dart';
part 'totals.g.dart';

@JsonSerializable()
class Totals {
  int? wallet = 0;
  int offers = 0;
  int realWallet = 0;
  List<Category> subCategories = [];
  List<BigCategory> categories = [];

  Totals(
      {required this.wallet,
        required this.offers,
        required this.realWallet,
        required this.subCategories,
        required this.categories});

  factory Totals.fromJson(Map<String, dynamic> json) => _$TotalsFromJson(json);

  Map<String, dynamic> toJson() => _$TotalsToJson(this);
}