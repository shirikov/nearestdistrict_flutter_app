// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cash_book.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CashBook _$CashBookFromJson(Map<String, dynamic> json) {
  return CashBook(
    amount: json['amount'] as int,
    organization: OfferOrganization.fromJson(
        json['organization'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CashBookToJson(CashBook instance) => <String, dynamic>{
      'amount': instance.amount,
      'organization': instance.organization,
    };
