class IdName {
  String id;
  String name;

  IdName({
    required this.id,
    required this.name,
  });
}