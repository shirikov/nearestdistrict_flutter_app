// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offer_organization.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OfferOrganization _$OfferOrganizationFromJson(Map<String, dynamic> json) {
  return OfferOrganization(
    id: json['id'] as String,
    name: json['name'] as String,
    latitude: (json['latitude'] as num).toDouble(),
    longitude: (json['longitude'] as num).toDouble(),
    subCategories: (json['subCategories'] as List<dynamic>)
        .map((e) => Category.fromJson(e as Map<String, dynamic>))
        .toList(),
    city: json['city'] as String?,
    address: json['address'] as String?,
    phone: json['phone'] as String?,
    online: json['online'] as bool?,
  );
}

Map<String, dynamic> _$OfferOrganizationToJson(OfferOrganization instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'subCategories': instance.subCategories,
      'city': instance.city,
      'address': instance.address,
      'phone': instance.phone,
      'online': instance.online,
    };
