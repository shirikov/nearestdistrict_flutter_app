// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Offer _$OfferFromJson(Map<String, dynamic> json) {
  return Offer(
    id: json['id'] as String,
    name: json['name'] as String,
    city: json['city'] as String,
    address: json['address'] as String,
    code: json['code'] as String,
    description: json['description'] as String,
    organization: OfferOrganization.fromJson(
        json['organization'] as Map<String, dynamic>),
    fullDay: json['fullDay'] as int,
    cashback: (json['cashback'] as num).toDouble(),
    cashbackMax: (json['cashbackMax'] as num).toDouble(),
    startDate: (json['startDate'] as num).toDouble(),
    finishDate: (json['finishDate'] as num).toDouble(),
    startTime: json['startTime'] as String,
    finishTime: json['finishTime'] as String,
  );
}

Map<String, dynamic> _$OfferToJson(Offer instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'city': instance.city,
      'address': instance.address,
      'code': instance.code,
      'description': instance.description,
      'organization': instance.organization,
      'fullDay': instance.fullDay,
      'cashback': instance.cashback,
      'cashbackMax': instance.cashbackMax,
      'startDate': instance.startDate,
      'finishDate': instance.finishDate,
      'startTime': instance.startTime,
      'finishTime': instance.finishTime,
    };
