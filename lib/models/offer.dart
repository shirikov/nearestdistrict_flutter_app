import 'package:nearestdistrict_app/models/offer_organization.dart';
import 'package:json_annotation/json_annotation.dart';

part 'offer.g.dart';

@JsonSerializable()
class Offer {
  String id = "";
  String name = "";
  String city = "";
  String address = "";
  String code = "";
  String description = "";
  OfferOrganization organization;

  int fullDay = 0;

  double cashback = 0;
  double cashbackMax = 0;

  double startDate = 0;
  double finishDate = 0;
  String startTime = "";
  String finishTime = "";

  Offer(
      {required this.id,
      required this.name,
      required this.city,
      required this.address,
      required this.code,
      required this.description,
      required this.organization,
      required this.fullDay,
      required this.cashback,
      required this.cashbackMax,
      required this.startDate,
      required this.finishDate,
      required this.startTime,
      required this.finishTime});

  factory Offer.fromJson(Map<String, dynamic> json) => _$OfferFromJson(json);

  Map<String, dynamic> toJson() => _$OfferToJson(this);
}
