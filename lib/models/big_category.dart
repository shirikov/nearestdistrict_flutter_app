import 'package:json_annotation/json_annotation.dart';

part 'big_category.g.dart';

@JsonSerializable()
class BigCategory {
  String id;
  String name;
  List<String> subCategories = [];

  BigCategory({
    required this.id,
    required this.name,
    required this.subCategories,
  });

  factory BigCategory.fromJson(Map<String, dynamic> json) =>
      _$BigCategoryFromJson(json);

  Map<String, dynamic> toJson() => _$BigCategoryToJson(this);
}
