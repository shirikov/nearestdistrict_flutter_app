// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'big_offer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BigOffer _$BigOfferFromJson(Map<String, dynamic> json) {
  return BigOffer(
    id: json['id'] as String,
    createdAt: (json['createdAt'] as num).toDouble(),
    usedAt: (json['usedAt'] as num).toDouble(),
    expiredAt: (json['expiredAt'] as num).toDouble(),
    offer: Offer.fromJson(json['offer'] as Map<String, dynamic>),
    sourceOrganization: OfferOrganization.fromJson(
        json['sourceOrganization'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$BigOfferToJson(BigOffer instance) => <String, dynamic>{
      'id': instance.id,
      'createdAt': instance.createdAt,
      'usedAt': instance.usedAt,
      'expiredAt': instance.expiredAt,
      'offer': instance.offer,
      'sourceOrganization': instance.sourceOrganization,
    };
