import 'package:nearestdistrict_app/models/fail_error.dart';
import 'package:json_annotation/json_annotation.dart';

part 'response_header.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class ResponseHeader<T> {
  @JsonKey(name: "status")
  String status = "";
  @JsonKey(name: "code")
  String code = "";
  @JsonKey(name: "data")
  T data;
  @JsonKey(name: "error")
  String? error = "";
  @JsonKey(name: "fail")
  List<FailError>? fail = [];

  ResponseHeader({
    required this.status,
    required this.code,
    required this.data,
    required this.error,
    required this.fail,
  });

  factory ResponseHeader.fromJson(Map<String, dynamic> json, T Function(Object? json) fromJsonT) => _$ResponseHeaderFromJson<T>(json, fromJsonT);

  Map<String, dynamic> toJson(Object Function(T value) toJsonT) => _$ResponseHeaderToJson(this, toJsonT);
}
