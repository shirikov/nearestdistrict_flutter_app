import 'package:geolocator/geolocator.dart';

class GeoPosition {
  double _latitude = 0;
  double _longitude = 0;
  double _timeGPS = 0;
  double _timeDevice = 0;
  double _accuracy = 0;

  GeoPosition.byCoordinates(double latitude, double longitude) {
    _latitude = latitude;
    _longitude = longitude;
  }

  GeoPosition(GeoPosition geoPosition) {
    _latitude = geoPosition.getLatitude();
    _longitude = geoPosition.getLongitude();
    _timeGPS = geoPosition.getTimeGPS();
    _timeDevice = geoPosition.getTimeDevice();
    _accuracy = geoPosition.getAccuracy();
  }

  void setLatitude(double latitude) => _latitude = latitude;
  void setLongitude(double longitude) => _longitude = longitude;
  void setTimeGPS(double timeGPS) => _timeGPS = timeGPS;
  void setTimeDevice(double timeDevice) => _timeDevice = timeDevice;
  void setAccuracy(double accuracy) => _accuracy = accuracy;

  double getLatitude() => _latitude;
  double getLongitude() => _longitude;
  double getTimeGPS() => _timeGPS;
  double getTimeDevice() => _timeDevice;
  double getAccuracy() => _accuracy;
}