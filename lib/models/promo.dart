import 'package:nearestdistrict_app/models/offer_organization.dart';
import 'package:json_annotation/json_annotation.dart';
part 'promo.g.dart';

@JsonSerializable()
class Promo {
  String id = "";
  String? name = "";
  String? description = "";
  String type = "";
  OfferOrganization organization;
  double cashback = 0;

  double? startDate = 0;
  double? finishDate = 0;
  int? fullDay = 0;
  String? startTime = "";
  String? finishTime = "";

  bool? isSelected = false;


  Promo(
      {required this.id,
        required this.name,
        required this.description,
        required this.type,
        required this.organization,
        required this.cashback,
        required this.startDate,
        required this.finishDate,
        required this.fullDay,
        required this.startTime,
        required this.finishTime,
        required this.isSelected});

  factory Promo.fromJson(Map<String, dynamic> json) => _$PromoFromJson(json);

  Map<String, dynamic> toJson() => _$PromoToJson(this);

  int getIconNumber() => organization.getCategory()!.icon;
}