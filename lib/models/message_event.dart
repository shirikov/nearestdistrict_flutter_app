class MessageEvent {
  String _message = "";
  Object _object = Object();
  String _wtd = "";

  MessageEvent(this._message);

  MessageEvent.setMW(this._message, this._wtd);

  MessageEvent.setMWO(this._message, this._wtd, this._object);

  void setMessage(String message) => _message = message;
  void setObject(Object object) => _object = object;
  void setWTD(String wtd) => _wtd = wtd;

  String getMessage() => _message;
  Object getObject() => _object;
  String getWTD() => _wtd;
}