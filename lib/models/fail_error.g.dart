// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fail_error.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FailError _$FailErrorFromJson(Map<String, dynamic> json) {
  return FailError(
    id: json['id'] as String,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$FailErrorToJson(FailError instance) => <String, dynamic>{
      'id': instance.id,
      'message': instance.message,
    };
