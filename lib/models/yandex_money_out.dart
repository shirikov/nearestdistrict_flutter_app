import 'package:json_annotation/json_annotation.dart';
part 'yandex_money_out.g.dart';
@JsonSerializable()
class YandexMoneyOut {
  @JsonKey(name: "type")
  String type = "";
  @JsonKey(name: "dstAccount")
  String dstAccount = "";
  @JsonKey(name: "amount")
  String amount = "0";

  YandexMoneyOut({
    required this.type,
    required this.dstAccount,
    required this.amount,
  });

  factory YandexMoneyOut.fromJson(Map<String, dynamic> json) => _$YandexMoneyOutFromJson(json);

  Map<String, dynamic> toJson() => _$YandexMoneyOutToJson(this);
}