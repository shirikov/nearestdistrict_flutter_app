// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_header.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseHeader<T> _$ResponseHeaderFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) {
  return ResponseHeader<T>(
    status: json['status'] as String,
    code: json['code'] as String,
    data: fromJsonT(json['data']),
    error: json['error'] as String?,
    fail: (json['fail'] as List<dynamic>?)
        ?.map((e) => FailError.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ResponseHeaderToJson<T>(
  ResponseHeader<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'status': instance.status,
      'code': instance.code,
      'data': toJsonT(instance.data),
      'error': instance.error,
      'fail': instance.fail,
    };
