// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'organization.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Organization _$OrganizationFromJson(Map<String, dynamic> json) {
  return Organization(
    id: json['id'] as String,
    name: json['name'] as String,
    city: json['city'] as String,
    address: json['address'] as String,
    phone: json['phone'] as String,
    latitude: (json['latitude'] as num?)?.toDouble(),
    longitude: (json['longitude'] as num?)?.toDouble(),
    inn: json['inn'] as int?,
    averageCheck: json['averageCheck'] as int,
    currentCashback: (json['currentCashback'] as num?)?.toDouble(),
    online: json['online'] as bool,
    subCategories: (json['subCategories'] as List<dynamic>)
        .map((e) => Category.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$OrganizationToJson(Organization instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'city': instance.city,
      'address': instance.address,
      'inn': instance.inn,
      'phone': instance.phone,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'averageCheck': instance.averageCheck,
      'currentCashback': instance.currentCashback,
      'online': instance.online,
      'subCategories': instance.subCategories,
    };
