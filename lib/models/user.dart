import 'package:json_annotation/json_annotation.dart';
import 'package:nearestdistrict_app/models/role.dart';
part 'user.g.dart';
@JsonSerializable()
class User {
  @JsonKey(name: "id")
  String id = "";
  @JsonKey(name: "firstName")
  String firstName = "";
  @JsonKey(name: "middleName")
  String? middleName = "";
  @JsonKey(name: "lastName")
  String? lastName = "";
  @JsonKey(name: "phone")
  String phone = "";
  @JsonKey(name: "role")
  List<Role> role;
  @JsonKey(name: "sex")
  String? sex = "";
  @JsonKey(name: "birthDate")
  double? birthDate = 0;

  User({
    required this.id,
    required this.firstName,
    required this.middleName,
    required this.lastName,
    required this.phone,
    required this.role,
    required this.sex,
    required this.birthDate,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  User setUser(User oldUser) {
    this.id = oldUser.id;
    this.firstName = oldUser.firstName;
    this.lastName = oldUser.lastName;
    this.middleName = oldUser.middleName;
    this.phone = oldUser.phone;
    this.sex = oldUser.sex;
    this.birthDate = oldUser.birthDate;

    return this;
  }

  bool isModified(User oldUser) {
    bool isMod = false;
    if (firstName != oldUser.firstName) isMod = true;
    if (middleName != oldUser.middleName) isMod = true;
    if (lastName != oldUser.lastName) isMod = true;
    if (phone != oldUser.phone) isMod = true;
    if (sex != oldUser.sex) isMod = true;
    if (birthDate != null) {
      if (oldUser.birthDate == 0) {
        isMod = true;
      } else {
        if (birthDate != oldUser.birthDate) isMod = true;
      }
    }
    return isMod;
  }

  String getFullName() => "$firstName $middleName $lastName".trim();
}
