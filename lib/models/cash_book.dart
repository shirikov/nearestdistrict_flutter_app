import 'package:nearestdistrict_app/models/offer_organization.dart';
import 'package:json_annotation/json_annotation.dart';
part 'cash_book.g.dart';

@JsonSerializable()
class CashBook {
  int amount = 0;
  OfferOrganization organization;

  CashBook({required this.amount, required this.organization});

  factory CashBook.fromJson(Map<String, dynamic> json) => _$CashBookFromJson(json);

  Map<String, dynamic> toJson() => _$CashBookToJson(this);
}