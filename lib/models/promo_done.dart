import 'package:json_annotation/json_annotation.dart';
part 'promo_done.g.dart';

@JsonSerializable()
class PromoDone {
  double wallet = 0;

  PromoDone({required this.wallet});

  factory PromoDone.fromJson(Map<String, dynamic> json) => _$PromoDoneFromJson(json);

  Map<String, dynamic> toJson() => _$PromoDoneToJson(this);
}