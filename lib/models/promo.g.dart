// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'promo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Promo _$PromoFromJson(Map<String, dynamic> json) {
  return Promo(
    id: json['id'] as String,
    name: json['name'] as String?,
    description: json['description'] as String?,
    type: json['type'] as String,
    organization: OfferOrganization.fromJson(
        json['organization'] as Map<String, dynamic>),
    cashback: (json['cashback'] as num).toDouble(),
    startDate: (json['startDate'] as num?)?.toDouble(),
    finishDate: (json['finishDate'] as num?)?.toDouble(),
    fullDay: json['fullDay'] as int?,
    startTime: json['startTime'] as String?,
    finishTime: json['finishTime'] as String?,
    isSelected: json['isSelected'] as bool?,
  );
}

Map<String, dynamic> _$PromoToJson(Promo instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'type': instance.type,
      'organization': instance.organization,
      'cashback': instance.cashback,
      'startDate': instance.startDate,
      'finishDate': instance.finishDate,
      'fullDay': instance.fullDay,
      'startTime': instance.startTime,
      'finishTime': instance.finishTime,
      'isSelected': instance.isSelected,
    };
