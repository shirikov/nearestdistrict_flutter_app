import 'package:json_annotation/json_annotation.dart';
part 'fail_error.g.dart';
@JsonSerializable()
class FailError {
  @JsonKey(name: "id")
  String id = '';
  @JsonKey(name: "message")
  String message = '';

  FailError({
    required this.id,
    required this.message,
  });

  factory FailError.fromJson(Map<String, dynamic> json) => _$FailErrorFromJson(json);

  Map<String, dynamic> toJson() => _$FailErrorToJson(this);
}