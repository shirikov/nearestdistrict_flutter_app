import 'big_offer.dart';

class Balloon {
  double _latitude;
  double _longitude;
  BigOffer _item;

  Balloon( this._latitude, this._longitude, this._item);

  void setLatitude(double latitude) => _latitude = latitude;
  void setLongitude(double longitude) => _longitude = longitude;
  void setBigOffer(BigOffer item) => _item = item;

  double getLatitude() => _latitude;
  double getLongitude() => _longitude;
  BigOffer getBigOffer() => _item;
}