// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cash_out_history.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CashOutHistory _$CashOutHistoryFromJson(Map<String, dynamic> json) {
  return CashOutHistory(
    date: (json['date'] as num).toDouble(),
    mainDate: (json['mainDate'] as num).toDouble(),
    amount: (json['amount'] as num).toDouble(),
    destinationType: json['destinationType'] as String,
    destination: json['destination'] as String,
    statusText: json['statusText'] as String,
    status: json['status'] as int,
  );
}

Map<String, dynamic> _$CashOutHistoryToJson(CashOutHistory instance) =>
    <String, dynamic>{
      'date': instance.date,
      'mainDate': instance.mainDate,
      'amount': instance.amount,
      'destinationType': instance.destinationType,
      'destination': instance.destination,
      'statusText': instance.statusText,
      'status': instance.status,
    };
