// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'yandex_money_out.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

YandexMoneyOut _$YandexMoneyOutFromJson(Map<String, dynamic> json) {
  return YandexMoneyOut(
    type: json['type'] as String,
    dstAccount: json['dstAccount'] as String,
    amount: json['amount'] as String,
  );
}

Map<String, dynamic> _$YandexMoneyOutToJson(YandexMoneyOut instance) =>
    <String, dynamic>{
      'type': instance.type,
      'dstAccount': instance.dstAccount,
      'amount': instance.amount,
    };
