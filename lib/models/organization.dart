import 'package:nearestdistrict_app/models/category.dart';
import 'package:json_annotation/json_annotation.dart';
part 'organization.g.dart';

@JsonSerializable()
class Organization {
  String id = "";
  String name = "";
  String city = "";
  String address = "";
  int? inn = 0;
  //double? discountPercentage = 0;
  String phone = "";
  double? latitude = 0;
  double? longitude = 0;
  //int? size = 0;
  //int colorIndex = 0;
  int averageCheck = 0;
  double? currentCashback = 0;
  bool online = false;
  List<Category> subCategories = [];

  Organization(
      {required this.id,
        required this.name,
        required this.city,
        required this.address,
        //required this.discountPercentage,
        required this.phone,
        required this.latitude,
        required this.longitude,
        //required this.size,
        required this.inn,
        //required this.colorIndex,
        required this.averageCheck,
        required this.currentCashback,
        required this.online,
        required this.subCategories});

  factory Organization.fromJson(Map<String, dynamic> json) => _$OrganizationFromJson(json);

  Map<String, dynamic> toJson() => _$OrganizationToJson(this);

  Category? getCategory() {
    if ((subCategories.isEmpty) || (subCategories.length == 0)) {
      return null;
    } else {
      return subCategories[0];
    }
  }
  int getIconNumber() => (getCategory() != null) ? getCategory()!.icon : 0;
}