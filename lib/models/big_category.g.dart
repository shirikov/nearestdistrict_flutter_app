// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'big_category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BigCategory _$BigCategoryFromJson(Map<String, dynamic> json) {
  return BigCategory(
    id: json['id'] as String,
    name: json['name'] as String,
    subCategories: (json['subCategories'] as List<dynamic>)
        .map((e) => e as String)
        .toList(),
  );
}

Map<String, dynamic> _$BigCategoryToJson(BigCategory instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'subCategories': instance.subCategories,
    };
