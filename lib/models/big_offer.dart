import 'package:nearestdistrict_app/models/offer.dart';
import 'package:nearestdistrict_app/models/offer_organization.dart';
import 'package:json_annotation/json_annotation.dart';
part 'big_offer.g.dart';

@JsonSerializable()
class BigOffer {
  String id = "";
  double createdAt = 0;
  double usedAt = 0;
  double expiredAt = 0;
  Offer offer;
  OfferOrganization sourceOrganization;

  BigOffer(
      {required this.id,
        required this.createdAt,
        required this.usedAt,
        required this.expiredAt,
        required this.offer,
        required this.sourceOrganization});

  factory BigOffer.fromJson(Map<String, dynamic> json) => _$BigOfferFromJson(json);

  Map<String, dynamic> toJson() => _$BigOfferToJson(this);

  void setLatitude(double lat) => offer.organization.setLatitude(lat);
  void setLongitude(double lon) => offer.organization.setLongitude(lon);

  double getLatitude() => offer.organization.latitude;
  double getLongitude() => offer.organization.longitude;

  String getName() => offer.name;
  String getDescription() => offer.description;

  int getIconNumber() => offer.organization.getIconNumber();
}