// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'promo_done.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PromoDone _$PromoDoneFromJson(Map<String, dynamic> json) {
  return PromoDone(
    wallet: (json['wallet'] as num).toDouble(),
  );
}

Map<String, dynamic> _$PromoDoneToJson(PromoDone instance) => <String, dynamic>{
      'wallet': instance.wallet,
    };
