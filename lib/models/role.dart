import 'package:json_annotation/json_annotation.dart';
part 'role.g.dart';
@JsonSerializable()
class Role {
  @JsonKey(name: "mobile_user_id")
  String mobileUserId = '';
  String role = '';

  Role({required this.mobileUserId, required this.role,});

  factory Role.fromJson(Map<String, dynamic> json) => _$RoleFromJson(json);

  Map<String, dynamic> toJson() => _$RoleToJson(this);
}