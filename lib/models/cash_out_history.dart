import 'package:json_annotation/json_annotation.dart';
part 'cash_out_history.g.dart';

@JsonSerializable()
class CashOutHistory {
  double date = 0;
  double mainDate = 0;
  double amount = 0;
  String destinationType = '';
  String destination = '';
  String statusText = '';
  int status = 0;

  CashOutHistory(
      {required this.date,
        required this.mainDate,
        required this.amount,
        required this.destinationType,
        required this.destination,
        required this.statusText,
        required this.status});

  factory CashOutHistory.fromJson(Map<String, dynamic> json) => _$CashOutHistoryFromJson(json);

  Map<String, dynamic> toJson() => _$CashOutHistoryToJson(this);
}