import 'package:json_annotation/json_annotation.dart';
part 'roles.g.dart';

@JsonSerializable()
class Roles {
  @JsonKey(name: "roles")
  List<String>? roles = [];

  Roles({required this.roles});

  bool isPresent() {
    bool isGood = false;
    if (roles == null) return false;
    if (roles!.length == 0) return false;
    for (String item in roles!) {
      if (item == "client" || item == "cashier") isGood = true;
    }
    return isGood;
  }

  bool isCashier() {
    bool isGood = false;
    if (roles == null) return false;
    if (roles!.length == 0) return false;
    for (String item in roles!) {
      if (item == "cashier") isGood = true;
    }
    return isGood;
  }

  bool isClient() {
    bool isGood = false;
    if (roles == null) return false;
    if (roles!.length == 0) return false;
    for (String item in roles!) {
      if (item == "client") isGood = true;
    }
    return isGood;
  }

  factory Roles.fromJson(Map<String, dynamic> json) => _$RolesFromJson(json);

  Map<String, dynamic> toJson() => _$RolesToJson(this);
}