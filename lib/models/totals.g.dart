// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'totals.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Totals _$TotalsFromJson(Map<String, dynamic> json) {
  return Totals(
    wallet: json['wallet'] as int?,
    offers: json['offers'] as int,
    realWallet: json['realWallet'] as int,
    subCategories: (json['subCategories'] as List<dynamic>)
        .map((e) => Category.fromJson(e as Map<String, dynamic>))
        .toList(),
    categories: (json['categories'] as List<dynamic>)
        .map((e) => BigCategory.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$TotalsToJson(Totals instance) => <String, dynamic>{
      'wallet': instance.wallet,
      'offers': instance.offers,
      'realWallet': instance.realWallet,
      'subCategories': instance.subCategories,
      'categories': instance.categories,
    };
