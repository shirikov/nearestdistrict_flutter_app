// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'promo_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PromoList _$PromoListFromJson(Map<String, dynamic> json) {
  return PromoList(
    code: json['code'] as String,
    offers: (json['offers'] as List<dynamic>)
        .map((e) => Promo.fromJson(e as Map<String, dynamic>))
        .toList(),
    clientChoice: (json['clientChoice'] as List<dynamic>?)
        ?.map((e) => Promo.fromJson(e as Map<String, dynamic>))
        .toList(),
    reminder: json['reminder'] as bool,
  );
}

Map<String, dynamic> _$PromoListToJson(PromoList instance) => <String, dynamic>{
      'code': instance.code,
      'offers': instance.offers,
      'clientChoice': instance.clientChoice,
      'reminder': instance.reminder,
    };
