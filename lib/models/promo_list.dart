import 'package:nearestdistrict_app/models/promo.dart';
import 'package:json_annotation/json_annotation.dart';
part 'promo_list.g.dart';

@JsonSerializable()
class PromoList {
  String code = "";
  List<Promo> offers = [];
  List<Promo>? clientChoice = [];
  bool reminder = false;


  PromoList({required this.code, required this.offers, required this.clientChoice, required this.reminder});

  factory PromoList.fromJson(Map<String, dynamic> json) => _$PromoListFromJson(json);

  Map<String, dynamic> toJson() => _$PromoListToJson(this);

  List<int> getMapPoints() {
    //TODO
    return [];
  }

  List<String> getSelected() {
    List<String> list = [];
    for (Promo promo in offers) {
      if (promo.isSelected!) {
        list.add(promo.id);
      }
    }
    return list;
  }
}