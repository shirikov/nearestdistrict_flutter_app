import 'package:nearestdistrict_app/models/category.dart';
import 'package:json_annotation/json_annotation.dart';
part 'offer_organization.g.dart';

@JsonSerializable()
class OfferOrganization {
  String id = "";
  String name = "";
  double latitude = 0;
  double longitude = 0;
  List<Category> subCategories = [];

  String? city = "";
  String? address = "";
  String? phone = "";

  bool? online = false;


  OfferOrganization(
      {required this.id,
        required this.name,
        required this.latitude,
        required this.longitude,
        required this.subCategories,
        required this.city,
        required this.address,
        required this.phone,
        required this.online});

  factory OfferOrganization.fromJson(Map<String, dynamic> json) => _$OfferOrganizationFromJson(json);

  Map<String, dynamic> toJson() => _$OfferOrganizationToJson(this);

  void setLatitude(double lat) => latitude = lat;
  void setLongitude(double lon) => longitude = lon;
  Category? getCategory() {
    if ((subCategories.isEmpty) || (subCategories.length == 0)) {
      return null;
    } else {
      return subCategories[0];
    }
  }
  int getIconNumber() => (getCategory() != null) ? getCategory()!.icon : 0;
}