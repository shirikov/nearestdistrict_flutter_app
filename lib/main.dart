import 'package:flutter/material.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/utils/app_strings.dart';
import 'package:nearestdistrict_app/view/view_find.dart';
import 'package:nearestdistrict_app/view/view_login.dart';
import 'package:nearestdistrict_app/view/view_main.dart';
import 'package:nearestdistrict_app/view/view_my_cash.dart';
import 'package:nearestdistrict_app/view/view_my_offers.dart';
import 'package:nearestdistrict_app/view/view_my_score.dart';
import 'package:nearestdistrict_app/view/view_new_cash_back.dart';
import 'package:nearestdistrict_app/view/view_slider.dart';
import 'package:nearestdistrict_app/view/view_splash.dart';

void main() async {
  runApp(MyApp());
  setDependencies();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: AppStrings.appName,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: ViewSplash(),
      routes: _setRout(),
    );
  }
}

Map<String, WidgetBuilder> _setRout() {
  return {
    'slider': (BuildContext context) => ViewSlider(),
    'login': (BuildContext context) => ViewLogin(),
    'main': (BuildContext context) => ViewMain(),
    'new cashback': (BuildContext context) => ViewNewCashBack(),
    'find': (BuildContext context) => ViewFind(),
    'my cash' : (BuildContext context) => ViewMyCash(),
    'my score' : (BuildContext context) => ViewMyScore(),
    'my offers' : (BuildContext context) => ViewMyOffers(),
  };
}