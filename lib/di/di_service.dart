import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:nearestdistrict_app/service/api_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

GetIt inject = GetIt.instance;

void setDependencies() async {
  WidgetsFlutterBinding.ensureInitialized();

  inject.registerSingleton(await SharedPreferences.getInstance());

  inject.registerSingleton(await ApiService.create());

  await Firebase.initializeApp();
  inject.registerSingleton(FirebaseAuth.instance);
}

void updateRetrofit() async {
  inject.unregister<ApiService>();
  inject.registerSingleton(await ApiService.create());
  print("Success re-register");
}
