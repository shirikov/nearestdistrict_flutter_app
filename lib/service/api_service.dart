import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:nearestdistrict_app/di/di_service.dart';
import 'package:nearestdistrict_app/models/big_offer.dart';
import 'package:nearestdistrict_app/models/cash_book.dart';
import 'package:nearestdistrict_app/models/cash_out_history.dart';
import 'package:nearestdistrict_app/models/login_data.dart';
import 'package:nearestdistrict_app/models/organization.dart';
import 'package:nearestdistrict_app/models/promo_done.dart';
import 'package:nearestdistrict_app/models/promo_list.dart';
import 'package:nearestdistrict_app/models/response_header.dart';
import 'package:nearestdistrict_app/models/roles.dart';
import 'package:nearestdistrict_app/models/totals.dart';
import 'package:nearestdistrict_app/models/user.dart';
import 'package:nearestdistrict_app/models/yandex_money_out.dart';
import 'package:nearestdistrict_app/utils/sp_keys.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:retrofit/retrofit.dart';
import 'package:shared_preferences/shared_preferences.dart';
part 'api_service.g.dart';

@RestApi(baseUrl: "http://test.rayonryadom.ru/api/")
abstract class ApiService {
  factory ApiService(Dio dio, {String baseUrl}) = _ApiService;

  static Future<ApiService> create() async {
    final dio = Dio(BaseOptions(contentType: "application/json"));
    var header = {
      //'device-id': await _getAndroidId(),
      'authorization': "Bearer " +
          (inject<SharedPreferences>().getString(SPKeys.userToken) != null
              ? inject<SharedPreferences>().getString(SPKeys.userToken)
              : '')
              .toString(),
    };
    dio.options.headers = header;
    dio.interceptors.add(PrettyDioLogger());
    return ApiService(dio);
  }

  @POST("auth/client-get-sms")
  Future<ResponseHeader<dynamic>> clientGetSmsCode(
      @Body() Map<String, String> data);

  @POST("auth/client-registration")
  Future<ResponseHeader<LoginData>> clientRegistration(
      @Body() Map<String, String> data);

  @POST("auth/client-login")
  Future<ResponseHeader<LoginData>> clientLogin(
      @Body() Map<String, String> data);

  @POST("auth/client-phone-check")
  Future<ResponseHeader<Roles>> phoneCheck(@Body() Map<String, String> data);

  @POST("user/change-profile")
  Future<ResponseHeader<LoginData>> saveProfile(@Body() User user);

  @POST("user/change-phone")
  Future<ResponseHeader<LoginData>> savePhone(@Body() Map<String, String> data);

  @POST("search/organization-by-coordinates")
  Future<ResponseHeader<List<Organization>>> getCacheBackByCoordinates(
      @Body() Map<String, double> location);

  @POST("search/organization-by-name")
  Future<ResponseHeader<List<Organization>>> getCacheBackByName(
      @Body() Map<String, String> name);

  @POST("cashback/get-code")
  Future<ResponseHeader<PromoList>> getCacheBackPromo(
      @Body() Map<String, String> organizationId);

  @GET("cashback/cashbook")
  Future<ResponseHeader<List<CashBook>>> getMyCash();

  @GET("main/user-real-wallet")
  Future<ResponseHeader<int>> getMyRealWallet();

  @POST("offer/save")
  Future<ResponseHeader<PromoDone>> saveSelectedPromo(
      @Body() Map<String, dynamic> name);

  @GET("offer/client-list-view")
  Future<ResponseHeader<List<BigOffer>>> getMyOffers();

  @GET("main/user-totals")
  Future<ResponseHeader<Totals>> getTotals();

  @POST("main/user-wallet")
  Future<ResponseHeader<int>> getWallet(@Body() Map<String, String> name);

  @POST("cashout/withdraw")
  Future<ResponseHeader<dynamic>> makeMoneyOut(
      @Body() YandexMoneyOut realMoney);

  @GET("cashout/status")
  Future<ResponseHeader<List<CashOutHistory>>> getCashOutHistory();
}

Future<String> _getAndroidId() async {
  final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
  return (await deviceInfoPlugin.androidInfo).androidId;
}