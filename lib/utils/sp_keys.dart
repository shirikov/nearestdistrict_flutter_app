class SPKeys {
  //shared preferences keys by data
  static const String needIntro = "needIntro";
  static const String userLogged = "userLogged";
  static const String userInfo = "userInfo";
  static const String userToken = "userToken";
  static const String userTotals = "totals";
  static const String userCurrentPositionLat = "latitude";
  static const String userCurrentPositionLon = "longitude";
}