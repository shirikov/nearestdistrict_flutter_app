class AppStrings {
  static const String appName = "Район Рядом";

  //view titles
  static const String titleViewMain = "Главная";
  static const String titleViewLogin = "Вход";
  static const String titleViewFind = "Поиск";
  static const String titleViewNewCashback = "Премии";
  static const String titleViewMyCash = "Кошелёк";
  static const String titleViewMyOffers = "Мой выбор";

  //for view header
  static const String leftTitle = "РАЙОН";
  static const String rightTitle = "РЯДОМ";

  //for button title
    //view slider
  static const String btnStart = "НАЧАТЬ";
    //view login
  static const String btnLoginGetCode = "Получить код";
  static const String btnLoginSignIn = "Войти";
  static const String btnLoginSignUp = "Зарегистрироваться";
    //view main
  static const String btnMainNewCacheBack = "ПРЕМИЯ ЗА ПОКУПКИ";
  static const String btnMainMyChoice = "МОЙ ВЫБОР";
  static const String btnMainMyScore = "МОИ БАЛЛЫ";
  static const String btnMainSearch = "НАЙТИ";
  static const String btnMainMyRealWallet = "МОЙ КОШЕЛЁК";
  static const String btnMainExit = "Выйти";
    //view find
  static const String btnFindMyScores = "МОИ БАЛЛЫ";
  static const String btnFindMyChoose = "МОЙ ВЫБОР";
  static const String btnFindMyStokes = "АКЦИИ";
  static const String btnFindMyOrganizations = "ОРГАНИЗАЦИИ";
    //view code cashback
  static const String btnReady = "ГОТОВО";
  static const String btnOk = "ОК";
    //view my cash
  static const String btnTransferMoney = "ПЕРЕВЕСТИ ДЕНЬГИ";
  static const String btnHistoryTransfer = "ИСТОРИЯ ПЕРЕВОДОВ";
  static const String btnYandexCash = "YANDEX КОШЕЛЁУ";
  static const String btnPhone = "ТЕЛЕФОН";
  static const String btnSend = "ОТПРАВИТЬ";
    //view my offers
  static const String btnShowOnMap = "ПОКАЗАТЬ НА КАРТЕ";
  static const String btnShowList = "ПОКАЗАТЬ СПИСОК";
  //view login
    //label TextField
  static const String labelFieldPhoneNumber = "Ваш номер телефона";
  static const String labelFieldCode = "Код из СМС";
  static const String labelFieldName = "Ваше Имя";
  static const String labelFieldPromoCode = "Промокод";
    //hint TextField
  static const String hintFieldPhoneNumber = "+7 (000) 000 00 00";
  static const String hintFieldCode = "Код из СМС";
  static const String hintFieldName = "Ваше Имя";
  static const String hintFieldPromoCode = "Промокод";
  //view new cashback
    //label TextField
  static const String labelFieldFind = "Поиск";
    //hint TextField
  static const String hintFieldFind = "Поиск";
  //view my cash
    //label TextField
  static const String labelFieldSum = "Сумма";
  static const String labelFieldYandexCash = "Yandex кошелёк";
    //hint TextField
  static const String hintFieldYandexCash = "Yandex кошелёк";
  //view find
    //label TextField
  static const String labelFieldSearch = "Поиск";
  //policy
  static const String policyBigShow1 = "Я согласен с ";
  static const String policyBigShow2 = " и даю согласие на обработку моих персональных данных.\nЯ согласен с Правилами участия в программе «РАЙОН РЯДОМ» и условиями ";
  static const String privacyPolicy = "Политикой конфиденциальности";
  static const String userAgreement = "Пользовательского соглашения";
  static const String urlPrivacyPolicy = "https://yadi.sk/i/TeDiOVo66L9AQA";
  static const String urlUserAgreement = "https://yadi.sk/i/47gkqeFk507QGQ";

  //http codes
  static const String codeSuccess = "200";

  //other
    //view login
  static const String loginActivityLoginLabel = "Введите, если знаете промокод от кассира/рекламы";
  static const String mainActivityAdditionalLabelBig1 = "Выберите точку продаж товаров/услуг, где вы сейчас находитесь или планируете посетить.\n\n";
  static const String mainActivityAdditionalLabelBig2 = "Выберите предложения от партнеров точки и система запомнит их в Моем выборе.\n\n";
  static const String mainActivityAdditionalLabelBig3 = "Начинайте с кнопки ПРЕМИИ ЗА ПОКУПКИ и включайте экономию от точек своего района.\n\n";
  static const String mainActivityAdditionalLabelBig4 = "Чем больше покупок у партнеров района, тем больше премий на вашем счете!";
  static const String newCacheBackEnterOrganisation = "Выберите название точки\nпродаж товаров/услуг";
    //view code
  static const String newCacheBackIDLabel = "ВАШ ИДЕНТИФИКАТОР\n";
  static const String newCacheBackIDLabelSecond = "Сообщите код кассиру\n";
  static const String newCacheBackIDLabelThird = "перед оплатой чека";
  static const String newCacheBackDoneScore = "Благодарим вас за использование РАЙОНРЯДОМ!\nИнформация о вашем выборе внесена в систему.";
    //view my cash
  static const String realWalletActivityPolicy1 = "Я СОГЛАСЕН(НА) с ";
  static const String realWalletActivityPolicy2 = "Условиями осуществления переводов денежных средств без открытия счета с использованием сервиса «Быстрый платеж через Яндекс.Деньги»\n\n";
  static const String realWalletActivityPolicy3 = "ВНИМАНИЕ: перевод возможен на сумму более 500 рублей. Деньги перечисляются на выбранные счета 1 раз в неделю после проверки чека.";
  static const String urlTransferConditions = "https://money.yandex.ru/pay/doc.xml?offerid=default";
    //view my offers
  static const String myOffersActivityTopLabel = "Посетите выбранные вами точки\nпродаж товаров/услуг и начислите баллы";
  static const String emptyList = "Данные отсутствуют";
    //view my score
  static const String myCashBackActivitySearchLabel = "Точки продаж товаров/услуг, где можно\nперевести баллы в деньги на вашем счете";
  //error message
  static const String error = "Ошибка";
  static const String errorSumMinAmount = "";
}