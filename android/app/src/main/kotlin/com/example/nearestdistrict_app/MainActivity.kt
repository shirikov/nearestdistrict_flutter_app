package com.example.nearestdistrict_app

import androidx.annotation.NonNull;
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine;

import com.yandex.mapkit.MapKitFactory

class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        MapKitFactory.setApiKey("76e90c13-088d-4844-92c1-f28c9875fa7e")
        super.configureFlutterEngine(flutterEngine)
    }
}
